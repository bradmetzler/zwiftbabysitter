﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;

namespace ZwiftBabysitter
{
    internal class LedScanner
    {
        private HashSet<String> discoveredIds = new HashSet<String>();  //Set of IDs of the lights we've found so far, so that we don't repeatedly notify about the same one if it responds to SSDP more than once

        private Action<LedLight> newLedBulbDiscoveredCallback;
        private Action<LedLight> newLedStripDiscoveredCallback;

        public volatile bool abort = false;
        private const int ssdpPort = 28154;     //Can be anything, really. Just needs to match on our send and receive clients below.

        public LedScanner(Action<LedLight> newLedBulbDiscoveredCallback, Action<LedLight> newLedStripDiscoveredCallback)
        {
            this.newLedBulbDiscoveredCallback = newLedBulbDiscoveredCallback;
            this.newLedStripDiscoveredCallback = newLedStripDiscoveredCallback;
        }

        public void Scan()
        {
            UdpClient udpSendClient = null;
            UdpClient udpRecvClient = null;

            try
            {
                //Send the SSDP broadcast message to discover lights on the network
                Log("Sending SSDP query broadcast...");
                udpSendClient = new UdpClient(ssdpPort);
                String ssdpSendData = "M-SEARCH * HTTP/1.1\r\n" +
                                      "MAN: \"ssdp:discover\"\r\n" +
                                      "ST: wifi_bulb\r\n";
                byte[] ssdpSendBytes = Encoding.ASCII.GetBytes(ssdpSendData);
                udpSendClient.Connect("255.255.255.255", 1982);
                udpSendClient.Send(ssdpSendBytes, ssdpSendBytes.Length);
                udpSendClient.Close();

                //Listen for SSDP responses from lights on the network
                IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, 0);
                udpRecvClient = new UdpClient(ssdpPort);
                udpRecvClient.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 1000);

                while (!abort)
                {
                    Log("Waiting for UDP packets...");

                    try
                    {
                        byte[] ssdpRecvBytes = udpRecvClient.Receive(ref endPoint);
                        ParseSsdpResponse(Encoding.ASCII.GetString(ssdpRecvBytes));
                    }
                    catch (Exception) { }
                }

                udpRecvClient.Close();
            }
            catch(Exception e)
            {
                abort = true;
                ZBState.Log(e.ToString());
                return;
            }
            finally
            {
                if (udpSendClient != null)
                    udpSendClient.Close();
                if (udpRecvClient != null)
                    udpRecvClient.Close();
            }
        }

        public void AbortScan()
        {
            Log("Stopping the LedScanner!");
            abort = true;
        }

        private void ParseSsdpResponse(String response)
        {
            Log("Got a UDP packet!");
            //Log("    " + response);

            String id;
            String model;
            String ip;
            int port;
            String name = null;

            Match idMatch = Regex.Match(response, "id: (.*)");
            if (idMatch.Success)
                id = idMatch.Groups[1].Value;
            else
            {
                ZBState.Log("Malformed SSDP response from LED, missing ID!");
                return;
            }

            Match modelMatch = Regex.Match(response, "model: (.*)");
            if (modelMatch.Success)
                model = modelMatch.Groups[1].Value;
            else
            {
                ZBState.Log("Malformed SSDP response from LED, missing model!");
                return;
            }

            Match locationMatch = Regex.Match(response, "Location: yeelight://([\\d\\.]+):(\\d+)");
            if (locationMatch.Success)
            {
                try
                {
                    ip = locationMatch.Groups[1].Value;
                    if(!int.TryParse(locationMatch.Groups[2].Value, out port))
                    {
                        ZBState.Log("Malformed SSDP response from LED, invalid port!");
                        return;
                    }
                }
                catch(Exception)
                {
                    ZBState.Log("Malformed SSDP response from LED, invalid location!");
                    return;
                }
            }
            else
            {
                ZBState.Log("Malformed SSDP response from LED, missing location!");
                return;
            }

            Match nameMatch = Regex.Match(response, "name: (.*)");
            if (nameMatch.Success)
                name = nameMatch.Groups[1].Value;

            if(!discoveredIds.Contains(id))
            {
                //This is the first response we've received from this light. Handle it and then ignore any more from the same light.
                discoveredIds.Add(id);

                LedLight light = new LedLight(id, name, ip, port);
                if(model.StartsWith("color") || model.StartsWith("ceiling") || model.StartsWith("bslamp"))
                {
                    //Bulb, Ceiling Dome Light, or BedSide Lamp
                    newLedBulbDiscoveredCallback(light);
                }
                else if(model.StartsWith("strip"))
                {
                    //LED Strip
                    newLedStripDiscoveredCallback(light);
                }
                else
                {
                    //Unknown
                    Log("Unrecognized LED device discovered: " + model + " (" + id + ")");
                }
            }
        }

        private void Log(String message)
        {
            //Most logging from this class is considered debug logging, so only log it if the user wants to see it
            if (ZBConfig.ledDebugLogEnabled)
                ZBState.Log(message);
        }
    }
}
