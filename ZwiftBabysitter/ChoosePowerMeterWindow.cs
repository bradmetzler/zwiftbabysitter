﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace ZwiftBabysitter
{
    public partial class ChoosePowerMeterWindow : Form
    {
        //These constants are because Windows Forms doesn't have a clean way to set the state of a progress bar
        private const int PBM_SETSTATE = 0x0400 + 16;
        private const int PBST_ERROR = 0x0002;

        private AntPowerMeterDevice selectedPowerMeter;

        private Thread scannerThread;                           //This thread is used for starting and stopping the ANT+ communication
        private AntPowerMeterScanner scanner;
        private List<Tuple<int, AntPowerMeterDevice>> availablePowerMeters; //index is also the index of the listbox on this form; value is a tuple of ANT+ channel# and the power meter connected to that channel
        private readonly object scannerLock = new object();
        private readonly object powerMeterLock = new object();

        private static readonly int scanTimerInterval = 500;    //Milliseconds
        private static readonly int scanTimerLength = 60;       //Seconds
        private long scanStart = 0;
        private System.Windows.Forms.Timer scanTimer;

        public ChoosePowerMeterWindow(AntPowerMeterDevice selectedPowerMeter)
        {
            InitializeComponent();

            pbScanProgress.Maximum = scanTimerLength;

            this.selectedPowerMeter = selectedPowerMeter;
            
            availablePowerMeters = new List<Tuple<int, AntPowerMeterDevice>>();

            scanTimer = new System.Windows.Forms.Timer();
            scanTimer.Interval = scanTimerInterval;
            scanTimer.Tick += new EventHandler(ScanTimerTick);
            scanTimer.Start();

            //ANT+ initialization can take a few seconds. Start the scanner in its own thread so that the UI doesn't hang
            lblScanProgress.Text = "Connecting to ANT+...";
            scannerThread = new Thread(new ThreadStart(ScanStartThreadProc));
            scannerThread.Start();
        }

        private void ScanTimerTick(Object myObject, EventArgs myEventArgs)
        {
            if (scanStart == 0)
                return;

            long currentTime = ZBState.CurrentTimeMillis();
            short elapsedSeconds = (short)((currentTime - scanStart) / 1000);

            if(elapsedSeconds >= scanTimerLength)
            {
                lblScanProgress.Text = "Done Scanning";
                pbScanProgress.Value = scanTimerLength;
                StopScanning();
            }
            else
            {
                lblScanProgress.Text = "Scanning for " + (scanTimerLength - elapsedSeconds) + " more seconds...";
                pbScanProgress.Value = elapsedSeconds;
            }

            //Refresh the on-screen list of available power meters
            lock (powerMeterLock)
            {
                int currentlySelectedIndex = listAvailablePowerMeters.SelectedIndex;
                listAvailablePowerMeters.Items.Clear();
                foreach (Tuple<int, AntPowerMeterDevice> availablePowerMeter in availablePowerMeters)
                    listAvailablePowerMeters.Items.Add(availablePowerMeter.Item2.GetPrettyFormat());
                if (currentlySelectedIndex > -1)
                    listAvailablePowerMeters.SetSelected(currentlySelectedIndex, true);
            }
        }

        private void ListAvailablePowerMeters_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnOk.Enabled = listAvailablePowerMeters.SelectedIndex > -1;
        }

        private void StopScanning()
        {
            if(scanTimer != null)
                scanTimer.Stop();

            if(scanner != null)
            {
                //ANT+ cleanup can take a few seconds. Stop the scanner in its own thread so that the UI doesn't hang
                scannerThread = new Thread(new ThreadStart(ScanStopThreadProc));
                scannerThread.Start();
            }
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            StopScanning();

            AntPowerMeterDevice x = availablePowerMeters[listAvailablePowerMeters.SelectedIndex].Item2;

            selectedPowerMeter.deviceNumber = x.deviceNumber;
            selectedPowerMeter.transmissionTypeId = x.transmissionTypeId;
            selectedPowerMeter.manufacturerId = x.manufacturerId;

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            StopScanning();

            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        public void ScanStartThreadProc()
        {
            lock(scannerLock)
            {
                scanner = new AntPowerMeterScanner(NewPowerMeterDiscovered, SearchStarted, AntConnectionError);
                scanner.StartScan();
            }
        }
        public void ScanStopThreadProc()
        {
            lock (scannerLock)
            {
                scanner.StopScan();
            }
        }
        public void NewPowerMeterDiscovered(int channel, AntPowerMeterDevice device)
        {
            lock(powerMeterLock)
            {
                availablePowerMeters.Add(new Tuple<int, AntPowerMeterDevice>(channel, device));
            }
        }

        public void SearchStarted()
        {
            scanStart = ZBState.CurrentTimeMillis();
        }

        public void AntConnectionError()
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new Action(AntConnectionError));
                return;
            }

            StopScanning();
            lblScanProgress.Text = "ANT+ Connection Error!";
            pbScanProgress.Value = scanTimerLength;
            SendMessage(pbScanProgress.Handle, PBM_SETSTATE, (IntPtr)PBST_ERROR, IntPtr.Zero);  //Put the progress bar into error state so that it shows as red
        }

        private void ChoosePowerMeterWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            StopScanning();
        }

        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int wMsg, IntPtr wParam, IntPtr lParam);
    }
}
