﻿namespace ZwiftBabysitter
{
    partial class ChoosePowerMeterWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChoosePowerMeterWindow));
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.pbScanProgress = new System.Windows.Forms.ProgressBar();
            this.lblScanProgress = new System.Windows.Forms.Label();
            this.listAvailablePowerMeters = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.Enabled = false;
            this.btnOk.Location = new System.Drawing.Point(84, 226);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(166, 226);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // pbScanProgress
            // 
            this.pbScanProgress.Location = new System.Drawing.Point(12, 25);
            this.pbScanProgress.Maximum = 60;
            this.pbScanProgress.Name = "pbScanProgress";
            this.pbScanProgress.Size = new System.Drawing.Size(300, 16);
            this.pbScanProgress.TabIndex = 2;
            // 
            // lblScanProgress
            // 
            this.lblScanProgress.Location = new System.Drawing.Point(12, 1);
            this.lblScanProgress.Name = "lblScanProgress";
            this.lblScanProgress.Size = new System.Drawing.Size(300, 21);
            this.lblScanProgress.TabIndex = 3;
            this.lblScanProgress.Text = "...";
            this.lblScanProgress.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // listAvailablePowerMeters
            // 
            this.listAvailablePowerMeters.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listAvailablePowerMeters.FormattingEnabled = true;
            this.listAvailablePowerMeters.ItemHeight = 20;
            this.listAvailablePowerMeters.Location = new System.Drawing.Point(12, 73);
            this.listAvailablePowerMeters.Name = "listAvailablePowerMeters";
            this.listAvailablePowerMeters.Size = new System.Drawing.Size(300, 144);
            this.listAvailablePowerMeters.TabIndex = 4;
            this.listAvailablePowerMeters.SelectedIndexChanged += new System.EventHandler(this.ListAvailablePowerMeters_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(300, 21);
            this.label1.TabIndex = 5;
            this.label1.Text = "Available Power Meters";
            this.label1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // ChoosePowerMeterWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(324, 261);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listAvailablePowerMeters);
            this.Controls.Add(this.lblScanProgress);
            this.Controls.Add(this.pbScanProgress);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ChoosePowerMeterWindow";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Choose Power Meter";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ChoosePowerMeterWindow_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ProgressBar pbScanProgress;
        private System.Windows.Forms.Label lblScanProgress;
        private System.Windows.Forms.ListBox listAvailablePowerMeters;
        private System.Windows.Forms.Label label1;
    }
}