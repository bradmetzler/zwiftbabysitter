﻿namespace ZwiftBabysitter
{
    partial class ChooseLightsWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChooseLightsWindow));
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.pbScanProgress = new System.Windows.Forms.ProgressBar();
            this.lblScanProgress = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.listAvailableStrips = new System.Windows.Forms.CheckedListBox();
            this.listAvailableBulbs = new System.Windows.Forms.CheckedListBox();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.Enabled = false;
            this.btnOk.Location = new System.Drawing.Point(84, 322);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(166, 322);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // pbScanProgress
            // 
            this.pbScanProgress.Location = new System.Drawing.Point(12, 25);
            this.pbScanProgress.Maximum = 60;
            this.pbScanProgress.Name = "pbScanProgress";
            this.pbScanProgress.Size = new System.Drawing.Size(300, 16);
            this.pbScanProgress.TabIndex = 2;
            // 
            // lblScanProgress
            // 
            this.lblScanProgress.Location = new System.Drawing.Point(12, 1);
            this.lblScanProgress.Name = "lblScanProgress";
            this.lblScanProgress.Size = new System.Drawing.Size(300, 21);
            this.lblScanProgress.TabIndex = 3;
            this.lblScanProgress.Text = "...";
            this.lblScanProgress.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(300, 21);
            this.label1.TabIndex = 5;
            this.label1.Text = "Available LED Bulbs";
            this.label1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(12, 184);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(300, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "Available LED Strips";
            this.label2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // listAvailableStrips
            // 
            this.listAvailableStrips.CheckOnClick = true;
            this.listAvailableStrips.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listAvailableStrips.FormattingEnabled = true;
            this.listAvailableStrips.Location = new System.Drawing.Point(12, 204);
            this.listAvailableStrips.Name = "listAvailableStrips";
            this.listAvailableStrips.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.listAvailableStrips.Size = new System.Drawing.Size(300, 109);
            this.listAvailableStrips.TabIndex = 8;
            this.listAvailableStrips.Click += new System.EventHandler(this.LightList_Click);
            // 
            // listAvailableBulbs
            // 
            this.listAvailableBulbs.CheckOnClick = true;
            this.listAvailableBulbs.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listAvailableBulbs.FormattingEnabled = true;
            this.listAvailableBulbs.Location = new System.Drawing.Point(12, 73);
            this.listAvailableBulbs.Name = "listAvailableBulbs";
            this.listAvailableBulbs.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.listAvailableBulbs.Size = new System.Drawing.Size(300, 109);
            this.listAvailableBulbs.TabIndex = 9;
            this.listAvailableBulbs.Click += new System.EventHandler(this.LightList_Click);
            // 
            // ChooseLightsWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(324, 353);
            this.Controls.Add(this.listAvailableBulbs);
            this.Controls.Add(this.listAvailableStrips);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblScanProgress);
            this.Controls.Add(this.pbScanProgress);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ChooseLightsWindow";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Choose Lights";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ChooseLightsWindow_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ProgressBar pbScanProgress;
        private System.Windows.Forms.Label lblScanProgress;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckedListBox listAvailableStrips;
        private System.Windows.Forms.CheckedListBox listAvailableBulbs;
    }
}