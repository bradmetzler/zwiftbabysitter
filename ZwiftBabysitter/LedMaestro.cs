﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ZwiftBabysitter
{
    internal class LedMaestro
    {
        private Action<int> ledConnectedCallback;

        private HashSet<String> missingLights;  //IDs of the lights the user wants to use. If this set becomes empty, we've found everything we're looking for.
        private List<LedLight> bulbs;
        private List<LedLight> strips;
        private bool powerMeterConnected = false;
        private bool configIsGood = false;      //User must have entered an FTP and chosen a power meter. Otherwise indicate that they have not.

        //LedScanner stuff here is for initially discovering the IPs of our lights, so that we can control them
        private Thread scannerThread;
        private LedScanner scanner;
        private readonly object ledLightLock = new object();
        private static readonly int scanTimerInterval = 10000;   //Milliseconds
        private System.Windows.Forms.Timer scanTimer;

        //Async tasks for network traffic to/from the lights
        private List<Task> networkAsyncs;
        private volatile bool networkAbort = false;
        private volatile bool networkSignal = false;

        public LedMaestro(Action<int> ledConnectedCallback)
        {
            this.ledConnectedCallback = ledConnectedCallback;

            configIsGood = ZBConfig.ftpWatts > 0 && ZBConfig.powerMeter.deviceNumber >= 0 && ZBConfig.powerMeter.transmissionTypeId >= 0;

            missingLights = new HashSet<String>();
            bulbs = new List<LedLight>();
            strips = new List<LedLight>();

            foreach(LedLight bulb in ZBConfig.ledBulbs)
                missingLights.Add(bulb.deviceId);
            foreach (LedLight strip in ZBConfig.ledStrips)
                missingLights.Add(strip.deviceId);
        }

        public void FindLeds()
        {
            //Start the asyncs that will be responsible for ongoing TCP communication with the lights
            NetworkListenAsync();

            //Timer to stop the LedScanner if we don't discover all of our lights
            scanTimer = new System.Windows.Forms.Timer();
            scanTimer.Interval = scanTimerInterval;
            scanTimer.Tick += new EventHandler(ScanTimerTick);
            scanTimer.Start();

            //LED Scanning blocks waiting for UDP packets. Start the scanner in its own thread so that the UI doesn't hang
            Log("Starting the LedScanner!");
            scanner = new LedScanner(NewBulbDiscovered, NewStripDiscovered);
            scannerThread = new Thread(new ThreadStart(scanner.Scan));
            scannerThread.Start();
        }

        public void ResetLedsAndDisconnect(bool appClosing)
        {
            networkAbort = true;

            //If we are disconnecting because ZwiftBabysitter is closing, don't start this in a thread, because we need to wait for the communication to finish.
            //Otherwise, start it in a thread so that we don't hold up the UI while we reset the lights.
            if (appClosing)
                ExecuteResetLedsAndDisconnect();
            else
                new Thread(new ThreadStart(ExecuteResetLedsAndDisconnect)).Start();
        }

        private void ExecuteResetLedsAndDisconnect()
        {
            //Reset all the lights to their initial state and close TCP connections
            List<Task> resetAsyncs = new List<Task>();

            foreach (LedLight bulb in bulbs)
                resetAsyncs.Add(bulb.ResetAndDisconnect());
            foreach (LedLight strip in strips)
                resetAsyncs.Add(strip.ResetAndDisconnect());

            bulbs.Clear();
            strips.Clear();

            Task.WaitAll(resetAsyncs.ToArray(), 5000);
        }

        public void NewBulbDiscovered(LedLight bulb)
        {
            NewLedDiscovered(bulb, bulbs, LedLightCommands.PulseWhiteCommand(true));
        }

        public void NewStripDiscovered(LedLight strip)
        {
            String connectionFlowCommand;
            if(configIsGood)
            {
                if (powerMeterConnected)
                    connectionFlowCommand = LedLightCommands.PowerMeterConnectedCommand();
                else
                    connectionFlowCommand = LedLightCommands.PulseWhiteCommand(false);
            }
            else
                connectionFlowCommand = LedLightCommands.ConfigErrorCommand();

            NewLedDiscovered(strip, strips, connectionFlowCommand);
        }

        private void NewLedDiscovered(LedLight led, List<LedLight> container, String connectionFlowCommand)
        {
            if (missingLights.Contains(led.deviceId))
            {
                Log("Found " + led.GetPrettyFormat() + " at " + led.ip + ":" + led.port);

                lock (ledLightLock)
                {
                    missingLights.Remove(led.deviceId);
                    container.Add(led);

                    if (missingLights.Count == 0)
                        StopScanning();
                }

                ledConnectedCallback(bulbs.Count + strips.Count);

                networkAsyncs.Add(led.Connect(connectionFlowCommand));
                networkSignal = true;
            }
        }

        private void ScanTimerTick(Object myObject, EventArgs myEventArgs)
        {
            StopScanning();
        }

        private void StopScanning()
        {
            if (scanTimer != null)
                scanTimer.Stop();

            //Signal the scanner thread to stop scanning
            if (scannerThread != null && scannerThread.IsAlive)
                scanner.AbortScan();
        }

        public void IndicatePowerMeterStatusChange(bool connected)
        {
            powerMeterConnected = connected;
            if(!configIsGood)
            {
                Log("Ignoring power meter connection status light indicator because config is invalid.");
                return;
            }

            Log("Power meter connection status light indicator: " + (connected ? "connected" : "disconnected"));
            if (connected)
            {
                //Strips should do a rainbow flow and stop on white at 50% brightness
                foreach (LedLight strip in strips)
                    strip.SendMessage(LedLightCommands.PowerMeterConnectedCommand());
            }
            else
            {
                //Bulbs go to white at 100% brightness
                foreach (LedLight bulb in bulbs)
                    bulb.SendMessage(LedLightCommands.SolidWhiteCommand(true));

                //Strips go to blinking red
                foreach (LedLight strip in strips)
                    strip.SendMessage(LedLightCommands.PowerMeterDisconnectedCommand());
            }
        }

        public void IndicateLowPowerMeterBattery()
        {
            foreach (LedLight bulb in bulbs)
                bulb.SendMessage(LedLightCommands.SolidPinkCommand());
            foreach (LedLight strip in strips)
                strip.SendMessage(LedLightCommands.SolidPinkCommand());
        }

        public void ChangeToZone(byte zone)
        {
            if(!configIsGood)
            {
                Log("Ignoring power zone status light change because config is invalid.");
                return;
            }

            String bulbCommand = null;
            String stripCommand = null;

            if(zone == 0)
            {
                bulbCommand = LedLightCommands.SolidWhiteCommand(true);
                stripCommand = LedLightCommands.SolidWhiteCommand(false);
            }
            else if(zone == 1)
            {
                bulbCommand = LedLightCommands.Zone1Command(true);
                stripCommand = LedLightCommands.Zone1Command(false);
            }
            else if(zone == 2)
            {
                bulbCommand = LedLightCommands.Zone2Command(true);
                stripCommand = LedLightCommands.Zone2Command(false);
            }
            else if (zone == 3)
            {
                bulbCommand = LedLightCommands.Zone3Command(true);
                stripCommand = LedLightCommands.Zone3Command(false);
            }
            else if (zone == 4)
            {
                bulbCommand = LedLightCommands.Zone4Command(true);
                stripCommand = LedLightCommands.Zone4Command(false);
            }
            else if (zone == 5)
            {
                bulbCommand = LedLightCommands.Zone5Command(true);
                stripCommand = LedLightCommands.Zone5Command(false);
            }
            else if (zone == 6)
            {
                bulbCommand = LedLightCommands.Zone6Command(true);
                stripCommand = LedLightCommands.Zone6Command(false);
            }

            if (bulbCommand != null)
            {
                foreach (LedLight bulb in bulbs)
                    bulb.SendMessage(bulbCommand);
            }
            if(stripCommand != null)
            {
                foreach (LedLight strip in strips)
                    strip.SendMessage(stripCommand);
            }

            /*foreach(LedLight strip in strips)
            {
                if (strip.deviceId.Equals("0x000000001c56e3d5"))
                    strip.SendMessage(LedLightCommands.SetNameCommand("PainCave LightStrip"));
            }*/
        }

        private async void NetworkListenAsync()
        {
            networkAsyncs = new List<Task>();
            
            //Add the signal async. This is how we can signal and abort the network loop below.
            Task signalAsync = NetworkSignalAsync();
            networkAsyncs.Add(signalAsync);

            try
            {
                while (true)
                {
                    Log("Waiting for a network task to finish...");
                    Task finishedTask = await Task.WhenAny(networkAsyncs);

                    //Remove the finished async from the list so that it doesn't keep triggering
                    networkAsyncs.Remove(finishedTask);

                    if (finishedTask == signalAsync)
                    {
                        if (networkAbort)
                            break;  //Received an abort signal. Jump out of the loop
                        else
                            networkAsyncs.Add(signalAsync = NetworkSignalAsync());  //Received a non-abort signal, probably because we have a new task we want to listen for, and we just need to start a new iteration to pick it up. Re-add a signal async for future signals/aborts.
                    }
                    else
                    {
                        Log("Finished network task");

                        try
                        {
                            LedLight receiver = ((Task<LedLight>)finishedTask).Result;
                            networkAsyncs.Add(receiver.TcpRead());
                        }
                        catch (Exception e)
                        {
                            ZBState.Log("Exception reading IP traffic. We probably lost connection to a light!");
                            ZBState.Log(e.ToString());
                        }
                    }
                }
            }
            catch(Exception e)
            {
                ZBState.Log("Exception in network loop! We probably lost connection to all lights!");
                ZBState.Log(e.ToString());
            }
        }

        private async Task<bool> NetworkSignalAsync()
        {
            while(!networkAbort && !networkSignal)
                await Task.Delay(500);

            networkSignal = false;
            return networkAbort;
        }

        private void Log(String message)
        {
            //Most logging from this class is considered debug logging, so only log it if the user wants to see it
            if (ZBConfig.ledDebugLogEnabled)
                ZBState.Log(message);
        }
    }
}
