﻿using System;
using System.Collections.Generic;
using System.Text.Json.Nodes;
using System.Text.RegularExpressions;

namespace ZwiftBabysitter
{
    internal static class LedLightCommands
    {
        //"Ambient" lights are those that provide the main source of light in the room (basically: bulbs). Typically their
        //effects are going to be more subdued, and we leave the more interesting colors/flows to the other "accent" lights.

        public static readonly int reqIdInitialState = 121;
        public static readonly int reqIdIgnoreResponse = 489;

        public static String RequestInitialStateCommand()
        {
            return "{\"id\": " + reqIdInitialState + ", \"method\": \"get_prop\", \"params\": [\"power\",\"flowing\",\"color_mode\",\"bright\",\"rgb\",\"ct\",\"hue\",\"sat\",\"flow_params\"]}";
        }

        public static String PowerOnCommand()
        {
            //Turn on smoothly over 200ms
            return "{\"id\": " + reqIdIgnoreResponse + ", \"method\": \"set_power\", \"params\": [\"on\", \"smooth\", 200]}";
        }

        public static String RgbCommand(int rgb, int brightness, int delay)
        {
            if (delay < 50)
                delay = 50;

            return "{\"id\": " + reqIdIgnoreResponse + ", \"method\": \"start_cf\", \"params\": [1, 1, \"" + delay + ",1," + rgb + "," + brightness + "\"]}";
        }

        public static String ColorTempCommand(int colorTemp, int brightness, int delay)
        {
            if (delay < 50)
                delay = 50;

            return "{\"id\": " + reqIdIgnoreResponse + ", \"method\": \"start_cf\", \"params\": [1, 1, \"" + delay + ",2," + colorTemp + "," + brightness + "\"]}";
        }

        public static String PulseWhiteCommand(bool ambient)
        {
            if(ambient)
                return "{\"id\": " + reqIdIgnoreResponse + ", \"method\": \"start_cf\", \"params\": [2, 1, \"500,2,5000,65,1000,2,5000,100\"]}";            //Pulse one time to 65% brightness and then to 100% and stop, all at 5000K color temp
            else
                return "{\"id\": " + reqIdIgnoreResponse + ", \"method\": \"start_cf\", \"params\": [0, 1, \"1000,1,16777215,100,1000,1,16777215,25\"]}";   //Continuously pulse white between 25% and 100% brightness
        }

        public static String SolidWhiteCommand(bool ambient)
        {
            if (ambient)
                return ColorTempCommand(5000, 100, 500);
            else
                return RgbCommand(16777215, 50, 500);
        }

        public static String SolidPinkCommand()
        {
            return RgbCommand(16716011, 100, 500);
        }

        public static String PowerMeterConnectedCommand()
        {
            //100% brightness rainbow red->orange->yellow->green->blue->purple->pink->white, and hold the white when done
            return "{\"id\": " + reqIdIgnoreResponse + ", \"method\": \"start_cf\", \"params\": [9, 1, \"200,1,16711680,100,200,1,16753920,100,200,1,16776960,100,200,1,65280,100,200,1,255,100,200,1,10494192,100,200,1,16716011,100,200,1,16777215,100,200,1,16777215,50\"]}";
        }

        public static String PowerMeterDisconnectedCommand()
        {
            //Slow red blink from 50% brightness to 0%
            return "{\"id\": " + reqIdIgnoreResponse + ", \"method\": \"start_cf\", \"params\": [0, 1, \"1000,1,0,1,1500,7,0,0,50,1,16711680,50\"]}";
        }

        public static String ConfigErrorCommand()
        {
            //Slow pink heartbeat
            return "{\"id\": " + reqIdIgnoreResponse + ", \"method\": \"start_cf\", \"params\": [0, 1, \"50,1,16716011,25,3500,7,0,0,50,1,16716011,50,50,7,0,0,50,1,16716011,25,50,7,0,0,50,1,16716011,50,50,7,0,0\"]}";
        }

        public static String SetNameCommand(string name)
        {
            //This is dumb. The Yeelight android app has a way to set the name of your lights, but it doesn't call this API to set the name that gets returned in the API. If you're OCD like me and want the name to show in API calls, you can use this command to set it.
            return "{\"id\": " + reqIdIgnoreResponse + ", \"method\": \"set_name\", \"params\": [\"" + name + "\"]}";
        }

        public static String Zone1Command(bool ambient)
        {
            if (ambient)
                return ColorTempCommand(5000, 50, 500);
            else
                return RgbCommand(16777215, 75, 500);
        }

        public static String Zone2Command(bool ambient)
        {
            if (ambient)
                return ColorTempCommand(5000, 25, 500);
            else
                return RgbCommand(3217663, 100, 500);
        }

        public static String Zone3Command(bool ambient)
        {
            if (ambient)
                return ColorTempCommand(5000, 25, 500);
            else
                return RgbCommand(65280, 100, 500);
        }

        public static String Zone4Command(bool ambient)
        {
            if (ambient)
                return ColorTempCommand(2700, 100, 500);
            else
                return RgbCommand(16776960, 100, 500);
        }

        public static String Zone5Command(bool ambient)
        {
            if (ambient)
                return RgbCommand(16753920, 100, 500);
            else
                return "{\"id\": " + reqIdIgnoreResponse + ", \"method\": \"start_cf\", \"params\": [0, 1, \"50,1,16753920,80,1700,7,0,0,50,1,16753920,100,100,7,0,0,50,1,16753920,80,50,7,0,0,50,1,16753920,100,50,7,0,0\"]}";    //2-second orange heartbeat 80-100% brightness
        }

        public static String Zone6Command(bool ambient)
        {
            if (ambient)
                return RgbCommand(16711680, 100, 500);
            else
                return "{\"id\": " + reqIdIgnoreResponse + ", \"method\": \"start_cf\", \"params\": [0, 1, \"50,1,16711680,75,1000,7,0,0,50,1,16711680,100,50,7,0,0,50,1,16711680,75,50,7,0,0,50,1,16711680,100,50,7,0,0\"]}";    //1-second red heartbeat 75-100% brightness
                //return "{\"id\": " + reqIdIgnoreResponse + ", \"method\": \"start_cf\", \"params\": [0, 1, \"50,1,16711680,50,450,7,0,0,250,1,16711680,80,100,7,0,0\"]}";    //Half-second red breathe 50-80% brightness
        }

        //This message tells us what the state of the light was before we started interacting with it.
        //We will parse this message and build a set of commands that we can send when we're done
        //that will put the light back into its initial state.
        public static List<String> GenerateInitialStateResetCommands(JsonNode state)
        {
            JsonArray stateParams = state["result"].AsArray();

            String power = stateParams[0].GetValue<String>();
            bool flowing = stateParams[1].GetValue<String>().Equals("1");
            int colorMode = int.Parse(stateParams[2].GetValue<String>());
            int brightness = int.Parse(stateParams[3].GetValue<String>());
            int rgb = int.Parse(stateParams[4].GetValue<String>());
            int colorTemp = int.Parse(stateParams[5].GetValue<String>());
            int hue = int.Parse(stateParams[6].GetValue<String>());
            int saturation = int.Parse(stateParams[7].GetValue<String>());
            String flowParams = stateParams[8].GetValue<String>();

            List<String> initialStateResetCommands = new List<String>();

            //First command sets the light back to its original color/flow state
            if (flowing)
            {
                //Flow params come to us as one big string, but when we send them back the first 2 parameters must be numbers.
                String fixedFlowParams = null;
                Match flowParamsMatch = Regex.Match(flowParams, "(\\d+),(\\d)+,(.*)");
                if (flowParamsMatch.Success)
                    fixedFlowParams = flowParamsMatch.Groups[1].Value + "," + flowParamsMatch.Groups[2].Value + ",\"" + flowParamsMatch.Groups[3].Value + "\"";

                initialStateResetCommands.Add("{\"id\": " + reqIdIgnoreResponse + ", \"method\": \"start_cf\", \"params\": [" + fixedFlowParams + "]}");
            }
            else
            {
                if (colorMode == 1)      //RGB mode
                    initialStateResetCommands.Add("{\"id\": " + reqIdIgnoreResponse + ", \"method\": \"set_rgb\", \"params\": [" + rgb + ", \"smooth\", 200]}");
                else if (colorMode == 2) //Color Temperature mode
                    initialStateResetCommands.Add("{\"id\": " + reqIdIgnoreResponse + ", \"method\": \"set_ct_abx\", \"params\": [" + colorTemp + ", \"smooth\", 200]}");
                else if (colorMode == 3) //Hue/Saturation mode
                    initialStateResetCommands.Add("{\"id\": " + reqIdIgnoreResponse + ", \"method\": \"set_hsv\", \"params\": [" + hue + ", " + saturation + ", \"smooth\", 200]}");
            }

            //Second command sets the brightness
            initialStateResetCommands.Add("{\"id\": " + reqIdIgnoreResponse + ", \"method\": \"set_bright\", \"params\": [" + brightness + ", \"smooth\", 200]}");

            //Third command turns the light on/off
            initialStateResetCommands.Add("{\"id\": " + reqIdIgnoreResponse + ", \"method\": \"set_power\", \"params\": [\"" + power + "\", \"smooth\", 200]}");

            Log("Commands to return to initial state:");
            foreach (String initialStateResetCommand in initialStateResetCommands)
                Log(initialStateResetCommand);

            return initialStateResetCommands;
        }

        private static void Log(String message)
        {
            //Most logging from this class is considered debug logging, so only log it if the user wants to see it
            if (ZBConfig.ledDebugLogEnabled)
                ZBState.Log(message);
        }
    }
}
