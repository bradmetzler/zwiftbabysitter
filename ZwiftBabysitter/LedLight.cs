﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Text.Json.Nodes;
using System.Threading.Tasks;

namespace ZwiftBabysitter
{
    public class LedLight
    {
        //General properties of a Yeelight LED device. These are saved to the ZBConfig.
        public String deviceId;
        public String deviceName;
        
        //Additional properties of a Yeelight LED device. These are discovered each time ZB starts, in case the IP has changed.
        public String ip;
        public int port;

        //For network communication with the Yeelight LED device.
        private TcpClient tcp;
        private List<String> initialStateResetCommands;

        public LedLight() { }
        public LedLight(String id, String name)
        {
            this.deviceId = id;
            this.deviceName = name;

            if(deviceId != null)
                deviceId = deviceId.Trim();
            if(deviceName != null)
                deviceName = deviceName.Trim();
        }
        public LedLight(String id, String name, String ip, int port) : this(id, name)
        {
            this.ip = ip;
            this.port = port;
        }

        public void SendMessage(String message)
        {
            byte[] bytes = Encoding.ASCII.GetBytes(message + "\r\n");
            
            if(tcp != null && tcp.Connected)
            {
                Log("Sending " + message + " to " + ip);
                tcp.Client.Send(bytes);
            }
            else
            {
                Log("Belaying message to " + ip);
            }
        }

        public async Task<LedLight> TcpRead()
        {
            ZBState.Log("Listening for TCP traffic from " + ip + "...");

            byte[] bytes = new byte[1024];
            int x = await tcp.Client.ReceiveAsync(new ArraySegment<byte>(bytes), 0);
            if (x > 0)
            {
                String[] messages = Encoding.ASCII.GetString(bytes, 0, x - 2).Split("\r\n"); //Last 2 characters are \r\n. We don't need them.
                Log("Got " + messages.Length + " messages from " + ip);
                foreach(String message in messages)
                    ProcessMessageFromLight(message);
            }

            return this;
        }

        public async Task<LedLight> Connect(String connectionFlowCommand)
        {
            //We need a couple of short delays because lights don't always respond if we connect and send the initial command too quickly after SSDP discovery
            await Task.Delay(250);
            
            Log("Connecting to light at " + ip + ":" + port + "...");
            tcp = new TcpClient();
            tcp.Connect(ip, port);
            
            await Task.Delay(250);
            SendMessage(LedLightCommands.RequestInitialStateCommand());

            await Task.Delay(250);
            SendMessage(LedLightCommands.PowerOnCommand());
            await Task.Delay(250);
            SendMessage(connectionFlowCommand);

            return this;
        }

        public async Task<bool> ResetAndDisconnect()
        {
            bool reset = false;
            if(initialStateResetCommands != null)
            {
                Log("Resetting " + ip + " to initial state...");

                foreach(String initialStateResetCommand in initialStateResetCommands)
                {
                    await Task.Delay(250);
                    SendMessage(initialStateResetCommand);
                }

                reset = true;
            }

            Log("Disconnecting " + ip + "...");
            if (tcp != null)
            {
                try
                {
                    tcp.Close();
                }
                catch { }
            }

            return reset;
        }

        private void ProcessMessageFromLight(String message)
        {
            Log("Processing " + message);
            JsonNode messageJson = JsonNode.Parse(message);
            JsonNode idNode = messageJson["id"];

            //These are informational messages that we can ignore
            if (idNode == null)
                return;

            int id = idNode.GetValue<int>();
            if (id == LedLightCommands.reqIdInitialState)
                initialStateResetCommands = LedLightCommands.GenerateInitialStateResetCommands(messageJson);
        }

        public String GetPrettyFormat()
        {
            if (deviceName != null && deviceName.Length > 0)
                return deviceName;
            else
                return deviceId;
        }

        public String GetDetailFormat()
        {
            if (deviceName != null && deviceName.Length > 0)
                return deviceName + " (" + deviceId + ")";
            else
                return deviceId;
        }

        private void Log(String message)
        {
            //Most logging from this class is considered debug logging, so only log it if the user wants to see it
            if (ZBConfig.ledDebugLogEnabled)
                ZBState.Log(message);
        }
    }
}
