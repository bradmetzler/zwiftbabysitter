﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace ZwiftBabysitter
{
    public partial class MainWindow : Form
    {
        Timer timer;
        long lastMainTimerTick = 0;
        Color colorUnknown = Color.FromArgb(175, 175, 175);
        Color colorDisabled = Color.FromArgb(215, 215, 215);
        Color colorRunning = Color.FromArgb(85, 200, 85);
        Color colorDone = Color.FromArgb(200, 85, 85);
        bool borderlessGamingFailedAdmin = false;

        public MainWindow()
        {
            InitializeComponent();
            toolStrip1.Renderer = new MyToolStripRenderer();
            Initialize();
            Application.ApplicationExit += new System.EventHandler(Shutdown);
        }
        private class MyToolStripRenderer : ToolStripProfessionalRenderer
        {
            protected override void OnRenderToolStripBorder(ToolStripRenderEventArgs e)
            {
                //Do nothing
            }
        }

        private void Initialize()
        {
            //Read configuration
            try
            {
                ZBConfig.LoadConfig();
            }
            catch(Exception ex)
            {
                ZBState.Log("Could not read config!");
                ZBState.Log(ex.ToString());
                OpenSettingsWindow(true);
                return;
            }

            toolStripHelpLedLegend.Enabled = ZBConfig.ledsEnabled;

            //Kill other babysitters
            AssassinateOtherBabysitters();

            //Start VirtualHere
            if(ZBConfig.virtualHereEnabled)
                FindVirtualhere(true);

            //Start BorderlessGaming
            if(ZBConfig.borderlessGamingEnabled)
                FindBorderlessGaming(true);

            //Start ZwiftLauncher
            FindZwiftLauncher(true);

            //Do initial tick
            CombinedTimerTick(null, null);

            //Start the timer
            timer = new Timer();
            timer.Interval = 100;
            timer.Tick += new EventHandler(CombinedTimerTick);
            timer.Start();

            StartAntLedController();
        }

        private void StartAntLedController()
        {
            //Start ANT+ LED controller, if enabled
            if (ZBConfig.ledsEnabled)
            {
                if (ZBConfig.powerMeter.deviceNumber > -1 && ZBConfig.powerMeter.transmissionTypeId > -1)
                {
                    ZBState.antLedController = new AntLedController();
                    ZBState.antLedController.Start();
                }
                else
                    ZBState.Log("Can't start ANT+ LED controller because no ANT+ power meter has been selected!");
            }
        }

        private void StopAntLedController(bool appClosing)
        {
            if(ZBState.antLedController != null)
                ZBState.antLedController.Stop(appClosing);
        }

        private void CombinedTimerTick(Object myObject, EventArgs myEventArgs)
        {
            UpdateLogWindow();

            if (ZBState.CurrentTimeMillis() - lastMainTimerTick >= ZBConfig.mainTimerInterval)
                MainTimerTick();
        }

        private void MainTimerTick()
        {
            lastMainTimerTick = ZBState.CurrentTimeMillis();

            //Reset safety switches
            if(killSwitchSafety2Btn.Enabled || killSwitchKillBtn.Enabled)
            {
                ZBState.Log("Resetting kill switch safeties...");
                killSwitchSafety2Btn.Enabled = false;
                killSwitchKillBtn.Enabled = false;
            }

            //Check if VirtualHere is running. If not, start it.
            if(ZBConfig.virtualHereEnabled)
                FindVirtualhere(true);

            //Check if BorderlessGaming is running. If not, start it.
            if(ZBConfig.borderlessGamingEnabled)
                FindBorderlessGaming(true);

            //Check if ZwiftLauncher is running
            FindZwiftLauncher(false);

            //Check if Zwift is running
            FindZwift();

            //If none are for 2 consecutive ticks, kill everything
            if(ZBState.zwiftDetected && ZBState.zwiftProcess == null)
            {
                if(ZBState.zwiftDeadOnLastCheck)
                    EndItAll();
                else
                    ZBState.zwiftDeadOnLastCheck = true;
            }
            else
                ZBState.zwiftDeadOnLastCheck = false;

            UpdateStatusText();
        }

        private void UpdateStatusText()
        {
            if (ZBState.virtualHereProcess == null)
            {
                if(ZBConfig.virtualHereEnabled)
                {
                    statusLabelVirtualhere.Text = "Not Found";
                    statusLabelVirtualhere.ForeColor = colorUnknown;
                }
                else
                {
                    statusLabelVirtualhere.Text = "Disabled";
                    statusLabelVirtualhere.ForeColor = colorDisabled;
                }
            }
            else
            {
                statusLabelVirtualhere.Text = "Running";
                statusLabelVirtualhere.ForeColor = colorRunning;
            }

            if (ZBState.borderlessGamingProcess == null)
            {
                if(borderlessGamingFailedAdmin)
                {
                    statusLabelBorderless.Text = "DENIED";
                    statusLabelBorderless.ForeColor = colorDone;
                }
                else if(ZBConfig.borderlessGamingEnabled)
                {
                    statusLabelBorderless.Text = "Not Found";
                    statusLabelBorderless.ForeColor = colorUnknown;
                }
                else
                {
                    statusLabelBorderless.Text = "Disabled";
                    statusLabelBorderless.ForeColor = colorDisabled;
                }
            }
            else
            {
                statusLabelBorderless.Text = "Running";
                statusLabelBorderless.ForeColor = colorRunning;
            }

            if (ZBState.zwiftLauncherProcess == null)
            {
                statusLabelLauncher.Text = "Not Found";
                statusLabelLauncher.ForeColor = colorUnknown;
            }
            else
            {
                statusLabelLauncher.Text = "Running";
                statusLabelLauncher.ForeColor = colorRunning;
            }

            if (ZBState.zwiftProcess == null)
            {
                if(ZBState.zwiftDetected)
                {
                    statusLabelZwift.Text = "Done";
                    statusLabelZwift.ForeColor = colorDone;
                }
                else
                {
                    statusLabelZwift.Text = "Not Found";
                    statusLabelZwift.ForeColor = colorUnknown;
                }
            }
            else
            {
                statusLabelZwift.Text = "Running";
                statusLabelZwift.ForeColor = colorRunning;
            }

            statusLabelTimestamp.Text = ZBState.Timestamp();
        }

        public void UpdateLogWindow()
        {
            String[] logs = ZBState.FetchLogs();

            foreach (String log in logs)
            {
                logBox.AppendText("\r\n" + log);
            }
        }

        private Process FindVirtualhere(bool startIfNotFound)
        {
            Process[] procs = Process.GetProcessesByName(ZBConfig.virtualHereProcName);
            if (procs.Length > 0)
            {
                if (ZBState.virtualHereProcess == null)
                    ZBState.Log("Found running VirtualHere.");

                ZBState.virtualHereProcess = procs[0];
                return ZBState.virtualHereProcess;
            }

            if (startIfNotFound)
            {
                ZBState.Log("Starting VirtualHere...");
                ZBState.virtualHereProcess = Process.Start(ZBConfig.virtualHereExe, ZBConfig.virtualHereArgs);
                return ZBState.virtualHereProcess;
            }

            if (ZBState.virtualHereProcess != null)
                ZBState.Log("VirtualHere has died.");
            ZBState.virtualHereProcess = null;
            return null;
        }

        private Process FindBorderlessGaming(bool startIfNotFound)
        {
            Process[] procs = Process.GetProcessesByName(ZBConfig.borderlessGamingProcName);
            if(procs.Length > 0)
            {
                if(ZBState.borderlessGamingProcess == null)
                    ZBState.Log("Found running BorderlessGaming.");

                ZBState.borderlessGamingProcess = procs[0];
                return ZBState.borderlessGamingProcess;
            }

            if(startIfNotFound)
            {
                if (borderlessGamingFailedAdmin)
                    return null;

                ZBState.Log("Starting BorderlessGaming...");
                
                try
                {
                    ZBState.borderlessGamingProcess = Process.Start(ZBConfig.borderlessGamingExe, ZBConfig.borderlessGamingArgs);
                }
                catch(Win32Exception)
                {
                    ZBState.Log("BorderlessGaming requires admin privileges. Are you running ZwiftBabysitter as admin?");
                    borderlessGamingFailedAdmin = true;
                    return null;
                }
                
                return ZBState.borderlessGamingProcess;
            }

            if (ZBState.borderlessGamingProcess != null)
                ZBState.Log("BorderlessGaming has died.");
            ZBState.borderlessGamingProcess = null;
            return null;
        }

        private Process FindZwiftLauncher(bool startIfNotFound)
        {
            Process[] procs = Process.GetProcessesByName(ZBConfig.zwiftLauncherProcName);
            if (procs.Length > 0)
            {
                if (ZBState.zwiftLauncherProcess == null)
                    ZBState.Log("Found running Zwift Launcher.");

                ZBState.zwiftLauncherProcess = procs[0];
                return ZBState.zwiftLauncherProcess;
            }

            if (startIfNotFound)
            {
                ZBState.Log("Starting Zwift Launcher...");
                ZBState.zwiftLauncherProcess = Process.Start(ZBConfig.zwiftLauncherExe, ZBConfig.zwiftLauncherArgs);
                return ZBState.zwiftLauncherProcess;
            }

            if (ZBState.zwiftLauncherProcess != null)
                ZBState.Log("Zwift Launcher has died.");
            ZBState.zwiftLauncherProcess = null;
            return null;
        }

        private Process FindZwift()
        {
            Process[] procs = Process.GetProcessesByName(ZBConfig.zwiftProcName);
            if (procs.Length > 0)
            {
                if(ZBState.zwiftProcess == null)
                {
                    ZBState.Log("Detected Zwift.");
                    ZBState.zwiftDetected = true;
                }

                ZBState.zwiftProcess = procs[0];
                return ZBState.zwiftProcess;
            }

            if(ZBState.zwiftProcess != null)
            {
                ZBState.Log("Zwift has died!");
                ZBState.Log("Killing other processes on next check, unless Zwift is re-detected!");
            }
            ZBState.zwiftProcess = null;
            return null;
        }

        private void AssassinateOtherBabysitters()
        {
            Process[] otherBabysitters = Process.GetProcessesByName(System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetEntryAssembly().Location));
            if(otherBabysitters.Length > 1)
            {
                Process me = Process.GetCurrentProcess();

                foreach(var otherBabysitter in otherBabysitters)
                {
                    if(otherBabysitter.Id != me.Id)
                        otherBabysitter.Kill();
                }
                ZBState.Log("Assassinated an existing babysitter!");
            }
        }

        private void OpenSettingsWindow(bool initOnClose)
        {
            ZBState.Log("Opening Settings window");
            StopAntLedController(false);

            SettingsWindow settingsWindow = new SettingsWindow();
            settingsWindow.FormClosed += (s, e) => SettingsWindowClosed(initOnClose, settingsWindow.DialogResult);
            settingsWindow.ShowDialog();
        }

        private void SettingsWindowClosed(bool initOnClose, DialogResult result)
        {
            ZBState.Log("Settings window closed: " + (result == DialogResult.OK ? "Save" : "Cancel"));

            if(initOnClose)
            {
                //This is true if we're on initial startup and the settings were invalid.
                //If new settings were saved, proceed with startup. Otherwise, close everything down.
                if (result == DialogResult.OK)
                    Initialize();
                else
                    Application.Exit();
            }
            else
            {
                //We fall into this else under normal circumstances if a user has updated their settings.
                //Don't need to initialize the entire app as above, but we should restart ANT+ in case
                //the user changed ANT+/LED settings and/or chose a new power meter
                StartAntLedController();
            }
        }

        private void EndItAll()
        {
            //Kill everything and quit
            try
            {
                try
                {
                    if (ZBState.zwiftLauncherProcess != null && !ZBState.zwiftLauncherProcess.HasExited)
                        ZBState.zwiftLauncherProcess.Kill();
                }
                catch (Win32Exception)
                {
                    ZBState.Log("Could not kill Zwift Launcher. Access denied?");
                }
                
                try
                {
                    if (ZBState.zwiftProcess != null && !ZBState.zwiftProcess.HasExited)
                        ZBState.zwiftProcess.Kill();
                }
                catch (Win32Exception)
                {
                    ZBState.Log("Could not kill Zwift. Access denied?");
                }

                try
                {
                    if (ZBState.borderlessGamingProcess != null && !ZBState.borderlessGamingProcess.HasExited)
                        ZBState.borderlessGamingProcess.Kill();
                }
                catch (Win32Exception)
                {
                    ZBState.Log("Could not kill Borderless Gaming. Access denied?");
                }

                try
                {
                    if (ZBState.virtualHereProcess != null && !ZBState.virtualHereProcess.HasExited)
                        ZBState.virtualHereProcess.Kill();
                }
                catch (Win32Exception)
                {
                    ZBState.Log("Could not kill VirtualHere. Access denied?");
                }

                Application.Exit();
            }
            catch(Exception ex)
            {
                ZBState.Log("Something bad happened while enditing it all:");
                ZBState.Log(ex.ToString());
            }
        }

        private void KillSwitchSafety1Btn_Click(object sender, EventArgs e)
        {
            ZBState.Log("KILL SWITCH Safety 1");
            killSwitchSafety2Btn.Enabled = true;
        }

        private void KillSwitchSafety2Btn_Click(object sender, EventArgs e)
        {
            ZBState.Log("KILL SWITCH Safety 2");
            killSwitchKillBtn.Enabled = true;
        }

        private void KillSwitchKillBtn_Click(object sender, EventArgs e)
        {
            EndItAll();
        }

        private void ToolStripToolsSettings_Click(object sender, EventArgs e)
        {
            OpenSettingsWindow(false);
        }

        private void ToolStripHelpAbout_Click(object sender, EventArgs e)
        {
            AboutWindow aboutWindow = new AboutWindow();
            aboutWindow.ShowDialog();
        }

        private void ToolStripHelpLedLegend_Click(object sender, EventArgs e)
        {
            LedLegendWindow ledLegendWindow = new LedLegendWindow();
            ledLegendWindow.ShowDialog();
        }

        private void Shutdown(object sender, EventArgs e)
        {
            StopAntLedController(true);
            ZBState.CloseLogFile();
        }
    }
}
