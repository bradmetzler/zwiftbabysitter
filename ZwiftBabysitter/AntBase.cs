﻿using ANT_Managed_Library;
using System;
using System.Text;

namespace ZwiftBabysitter
{
    internal abstract class AntBase
    {
        protected ANT_Device antDevice;
        protected ANT_Channel[] channels;

        protected Boolean isRunning = false;

        protected abstract void ReceiveAntPageData(byte channelNum, ANT_ChannelID channelId, byte[] pageData);

        protected abstract void ChannelClosed(byte channelNum);

        private void Log(String message)
        {
            //All logging from this class is considered ANT+ debug logging, so only log it if the user wants to see it
            if(ZBConfig.antDebugLogEnabled)
                ZBState.Log(message);
        }

        protected void InitAntDevice()
        {
            try
            {
                uint numDevices = ANT_Common.getNumDetectedUSBDevices();

                if(numDevices < 1)
                {
                    Log("No ANT+ sticks detected!");
                    throw new Exception("No ANT+ sticks detected!");
                }

                ZBState.Log(numDevices + " ANT+ sticks detected");

                byte deviceNum = (byte)(numDevices - 1);
                if (ZBConfig.antUsbId >= 0 && ZBConfig.antUsbId < numDevices)
                    deviceNum = (byte)ZBConfig.antUsbId;

                ZBState.Log("Attempting to connect to ANT+ USB device " + deviceNum + "...");

                try
                {
                    //Zwift appears to always connect to device #0. We will, therefore, connect to the last available device (unless the user specifies a device number) in an attempt
                    //to avoid connecting to the same one. Connecting to the same one results in neither Zwift nor ZwiftBabysitter getting any ANT+ connections. We MUST use separate devices.
                    antDevice = new ANT_Device(deviceNum, 57600u); //Attempt first with baud rate 57600.
                }
                catch(Exception)
                {
                    try
                    {
                        antDevice = new ANT_Device(deviceNum, 50000u); //Attempt again with baud rate 50000.
                    }
                    catch (Exception)
                    {
                        Log("Unable to connect to ANT+ USB device!");
                        throw;
                    }
                }
                
                isRunning = true;
                PrintUsbInformation();
                antDevice.deviceResponse += new ANT_Device.dDeviceResponseHandler(DeviceResponse);    //Add device response callback to receive protocol event messages
                Log("ANT Initialization was successful!");

                Log("Resetting module...");
                antDevice.ResetSystem();            //Soft reset
                System.Threading.Thread.Sleep(500); //Delay 500ms after a reset

                //If you call the setup functions specifying a wait time, you can check the return value for success or failure of the command
                //This function is blocking - the thread will be blocked while waiting for a response.
                //500ms is usually a safe value to ensure you wait long enough for any response
                //If you do not specify a wait time, the command is simply sent, and you have to monitor the protocol events for the response,
                Log("Setting network key...");
                if (antDevice.setNetworkKey(AntPowerMeterDevice.networkNum, AntPowerMeterDevice.networkKey, 500))
                    Log("Network key set");
                else
                    throw new Exception("Error configuring network key");

                //Extended messages are not supported in all ANT devices, so
                //we will not wait for the response here, and instead will monitor 
                //the protocol events
                Log("Enabling extended messages...");
                antDevice.enableRxExtendedMessages(true);
            }
            catch (Exception ex)
            {
                if (antDevice == null)    //Unable to connect to ANT+
                {
                    throw new Exception("Could not connect to any device.\nDetails: \n   " + ex.Message);
                }
                else
                {
                    throw new Exception("Error connecting to ANT+: " + ex.Message);
                }
            }
        }

        private void PrintUsbInformation()
        {
            //Print out information about the device we are connected to
            Log("USB Device Description");

            //Retrieve info
            Log(String.Format("   VID: 0x{0:x}", antDevice.getDeviceUSBVID()));
            Log(String.Format("   PID: 0x{0:x}", antDevice.getDeviceUSBPID()));
            Log(String.Format("   Product Description: {0}", antDevice.getDeviceUSBInfo().printProductDescription()));
            Log(String.Format("   Serial String: {0}", antDevice.getDeviceUSBInfo().printSerialString()));
        }

        protected void CleanUpANT()
        {
            //Clean up ANT
            if (antDevice != null)
            {
                Log("Disconnecting ANT device...");
                isRunning = false;
                try
                {
                    ANT_Device.shutdownDeviceInstance(ref antDevice);  //Close down the device completely and completely shut down all communication
                }
                catch(Exception)
                {

                }
            }
        }

        protected void OpenAntChannel(int channelNum, ushort deviceNum, byte transmissionType)
        {
            ANT_Channel channel = channels[channelNum];
            if (channel == null)
            {
                channel = antDevice.getChannel(channelNum);   //Get channel from ANT device
                channel.channelResponse += new dChannelResponseHandler(ChannelResponse);  //Add channel response callback to receive channel event messages

                channels[channelNum] = channel;
            }

            Log("Assigning channel " + channelNum + "...");
            if (!channel.assignChannel(ANT_ReferenceLibrary.ChannelType.BASE_Slave_Receive_0x00, AntPowerMeterDevice.networkNum, 500))
                throw new Exception("Error assigning channel");

            Log("Setting Channel ID " + channelNum + "...");
            if (!channel.setChannelID(deviceNum, false, AntPowerMeterDevice.deviceTypeId, transmissionType, 500))  //Not using pairing bit
                throw new Exception("Error configuring Channel ID");

            Log("Setting Radio Frequency " + channelNum + "...");
            if (!channel.setChannelFreq(AntPowerMeterDevice.radioFrequency, 500))
                throw new Exception("Error configuring Radio Frequency");

            Log("Setting Channel Period " + channelNum + "...");
            if (!channel.setChannelPeriod(AntPowerMeterDevice.channelPeriod, 500))
                throw new Exception("Error configuring Channel Period");

            if(deviceNum == 0 || transmissionType == 0)
            {
                channel.setChannelSearchPriority(0, 500);
                channel.setChannelSearchTimeout(0, 500);
                channel.setLowPrioritySearchTimeout(30, 500);
            }

            Log("Opening channel" + channelNum + "...");
            if (channel.openChannel(500))
                Log("Channel " + channelNum + " opened");
            else
                throw new Exception("Error opening channel");
        }

        protected void CloseAntChannel(int channelNum)
        {
            ANT_Channel channel = channels[channelNum];
            channels[channelNum] = null;

            Log("Closing channel " + channelNum + "...");
            if (!channel.closeChannel(500))
                throw new Exception("Error closing channel");
        }

        ////////////////////////////////////////////////////////////////////////////////
        //ChannelResponse
        //
        //Called whenever a channel event is recieved. 
        //
        //response: ANT message
        ////////////////////////////////////////////////////////////////////////////////
        private void ChannelResponse(ANT_Response response)
        {
            byte channelNum = response.antChannel;
            String preamble = "(ANT+ " + channelNum + ") ";

            try
            {
                switch ((ANT_ReferenceLibrary.ANTMessageID)response.responseID)
                {
                    case ANT_ReferenceLibrary.ANTMessageID.RESPONSE_EVENT_0x40:
                        {
                            switch (response.getChannelEventCode())
                            {
                                //This event indicates that a message has just been
                                //sent over the air. We take advantage of this event to set
                                //up the data for the next message period.   
                                case ANT_ReferenceLibrary.ANTEventID.EVENT_TX_0x03:
                                    {
                                        Log(preamble + "SOMEHOW GOT A TX EVENT?!");
                                        break;
                                    }
                                case ANT_ReferenceLibrary.ANTEventID.EVENT_RX_SEARCH_TIMEOUT_0x01:
                                    {
                                        Log(preamble + "Search Timeout");
                                        break;
                                    }
                                case ANT_ReferenceLibrary.ANTEventID.EVENT_RX_FAIL_0x02:
                                    {
                                        //Log(preamble + "Rx Fail");
                                        break;
                                    }
                                case ANT_ReferenceLibrary.ANTEventID.EVENT_TRANSFER_RX_FAILED_0x04:
                                    {
                                        Log(preamble + "Burst receive has failed");
                                        break;
                                    }
                                case ANT_ReferenceLibrary.ANTEventID.EVENT_TRANSFER_TX_COMPLETED_0x05:
                                    {
                                        Log(preamble + "Transfer Completed");
                                        break;
                                    }
                                case ANT_ReferenceLibrary.ANTEventID.EVENT_TRANSFER_TX_FAILED_0x06:
                                    {
                                        Log(preamble + "Transfer Failed");
                                        break;
                                    }
                                case ANT_ReferenceLibrary.ANTEventID.EVENT_CHANNEL_CLOSED_0x07:
                                    {
                                        //This event should be used to determine that the channel is closed.
                                        Log(preamble + "Channel Closed");
                                        Log(preamble + "Unassigning Channel...");
                                        if (channels[channelNum].unassignChannel(500))
                                        {
                                            Log(preamble + "Unassigned Channel");
                                        }
                                        ChannelClosed(channelNum);
                                        break;
                                    }
                                case ANT_ReferenceLibrary.ANTEventID.EVENT_RX_FAIL_GO_TO_SEARCH_0x08:
                                    {
                                        Log(preamble + "Go to Search");
                                        break;
                                    }
                                case ANT_ReferenceLibrary.ANTEventID.EVENT_CHANNEL_COLLISION_0x09:
                                    {
                                        Log(preamble + "Channel Collision");
                                        break;
                                    }
                                case ANT_ReferenceLibrary.ANTEventID.EVENT_TRANSFER_TX_START_0x0A:
                                    {
                                        Log(preamble + "Burst Started");
                                        break;
                                    }
                                default:
                                    {
                                        Log(preamble + "Unhandled Channel Event " + response.getChannelEventCode());
                                        break;
                                    }
                            }
                            break;
                        }
                    case ANT_ReferenceLibrary.ANTMessageID.BROADCAST_DATA_0x4E:
                    case ANT_ReferenceLibrary.ANTMessageID.ACKNOWLEDGED_DATA_0x4F:
                    case ANT_ReferenceLibrary.ANTMessageID.BURST_DATA_0x50:
                    case ANT_ReferenceLibrary.ANTMessageID.EXT_BROADCAST_DATA_0x5D:
                    case ANT_ReferenceLibrary.ANTMessageID.EXT_ACKNOWLEDGED_DATA_0x5E:
                    case ANT_ReferenceLibrary.ANTMessageID.EXT_BURST_DATA_0x5F:
                        {
                            if(response.isExtended())
                            {
                                StringBuilder broadcastDataLogMessage = new StringBuilder();
                                broadcastDataLogMessage.Append(preamble);

                                ANT_ChannelID chID = response.getDeviceIDfromExt();    //Channel ID of the device we just received a message from
                                broadcastDataLogMessage.Append("ID(" + chID.deviceNumber.ToString() + "," + chID.deviceTypeID.ToString() + "," + chID.transmissionTypeID.ToString() + ") - ");

                                byte[] payload = response.getDataPayload();
                                broadcastDataLogMessage.Append("Page " + BitConverter.ToString(payload, 0, 1) + " - ");
                                broadcastDataLogMessage.Append("Data " + BitConverter.ToString(payload, 1));

                                Log(broadcastDataLogMessage.ToString());

                                ReceiveAntPageData(response.antChannel, chID, payload);
                            }
                            else
                            {
                                Log(preamble + "Unexpected non-extended data!");
                            }
                            
                            break;
                        }
                    default:
                        {
                            Log(preamble + "Unknown Message " + response.responseID);
                            break;
                        }
                }
            }
            catch (Exception e)
            {
                ZBState.Log(preamble + "Channel response processing failed with exception: " + e.Message);
            }
        }


        ////////////////////////////////////////////////////////////////////////////////
        //DeviceResponse
        //
        //Called whenever a message is received from ANT unless that message is a 
        //channel event message. 
        //
        //response: ANT message
        ////////////////////////////////////////////////////////////////////////////////
        private void DeviceResponse(ANT_Response response)
        {
            try
            {
                switch ((ANT_ReferenceLibrary.ANTMessageID)response.responseID)
                {
                    case ANT_ReferenceLibrary.ANTMessageID.STARTUP_MESG_0x6F:
                        {
                            Log("RESET Complete, reason: ");

                            byte ucReason = response.messageContents[0];

                            if (ucReason == (byte)ANT_ReferenceLibrary.StartupMessage.RESET_POR_0x00)
                                Log("RESET_POR");
                            if (ucReason == (byte)ANT_ReferenceLibrary.StartupMessage.RESET_RST_0x01)
                                Log("RESET_RST");
                            if (ucReason == (byte)ANT_ReferenceLibrary.StartupMessage.RESET_WDT_0x02)
                                Log("RESET_WDT");
                            if (ucReason == (byte)ANT_ReferenceLibrary.StartupMessage.RESET_CMD_0x20)
                                Log("RESET_CMD");
                            if (ucReason == (byte)ANT_ReferenceLibrary.StartupMessage.RESET_SYNC_0x40)
                                Log("RESET_SYNC");
                            if (ucReason == (byte)ANT_ReferenceLibrary.StartupMessage.RESET_SUSPEND_0x80)
                                Log("RESET_SUSPEND");
                            break;
                        }
                    case ANT_ReferenceLibrary.ANTMessageID.VERSION_0x3E:
                        {
                            Log("VERSION: " + new ASCIIEncoding().GetString(response.messageContents));
                            break;
                        }
                    case ANT_ReferenceLibrary.ANTMessageID.RESPONSE_EVENT_0x40:
                        {
                            switch (response.getMessageID())
                            {
                                case ANT_ReferenceLibrary.ANTMessageID.CLOSE_CHANNEL_0x4C:
                                    {
                                        if (response.getChannelEventCode() == ANT_ReferenceLibrary.ANTEventID.CHANNEL_IN_WRONG_STATE_0x15)
                                        {
                                            byte channelNum = response.antChannel;
                                            String preamble = "(ANT+ " + channelNum + ") ";

                                            Log(preamble + "Channel is already closed");
                                            Log(preamble + "Unassigning Channel...");
                                            if (channels[channelNum].unassignChannel(500))
                                            {
                                                Log(preamble + "Unassigned Channel");
                                            }
                                            ChannelClosed(channelNum);
                                        }
                                        break;
                                    }
                                case ANT_ReferenceLibrary.ANTMessageID.NETWORK_KEY_0x46:
                                case ANT_ReferenceLibrary.ANTMessageID.ASSIGN_CHANNEL_0x42:
                                case ANT_ReferenceLibrary.ANTMessageID.CHANNEL_ID_0x51:
                                case ANT_ReferenceLibrary.ANTMessageID.CHANNEL_RADIO_FREQ_0x45:
                                case ANT_ReferenceLibrary.ANTMessageID.CHANNEL_MESG_PERIOD_0x43:
                                case ANT_ReferenceLibrary.ANTMessageID.OPEN_CHANNEL_0x4B:
                                case ANT_ReferenceLibrary.ANTMessageID.UNASSIGN_CHANNEL_0x41:
                                case ANT_ReferenceLibrary.ANTMessageID.SET_SEARCH_PRIORITY_LEVEL_0x75:
                                case ANT_ReferenceLibrary.ANTMessageID.CHANNEL_SEARCH_TIMEOUT_0x44:
                                case ANT_ReferenceLibrary.ANTMessageID.SET_LP_SEARCH_TIMEOUT_0x63:
                                    {
                                        if (response.getChannelEventCode() != ANT_ReferenceLibrary.ANTEventID.RESPONSE_NO_ERROR_0x00)
                                        {
                                            Log(String.Format("Error {0} configuring {1}", response.getChannelEventCode(), response.getMessageID()));
                                        }
                                        break;
                                    }
                                case ANT_ReferenceLibrary.ANTMessageID.RX_EXT_MESGS_ENABLE_0x66:
                                    {
                                        if (response.getChannelEventCode() == ANT_ReferenceLibrary.ANTEventID.INVALID_MESSAGE_0x28)
                                        {
                                            Log("Extended messages not supported in this ANT product");
                                            break;
                                        }
                                        else if (response.getChannelEventCode() != ANT_ReferenceLibrary.ANTEventID.RESPONSE_NO_ERROR_0x00)
                                        {
                                            Log(String.Format("Error {0} configuring {1}", response.getChannelEventCode(), response.getMessageID()));
                                            break;
                                        }
                                        Log("Extended messages enabled");
                                        break;
                                    }
                                case ANT_ReferenceLibrary.ANTMessageID.REQUEST_0x4D:
                                    {
                                        if (response.getChannelEventCode() == ANT_ReferenceLibrary.ANTEventID.INVALID_MESSAGE_0x28)
                                        {
                                            Log("Requested message not supported in this ANT product");
                                            break;
                                        }
                                        break;
                                    }
                                default:
                                    {
                                        Log("Unhandled response " + response.getChannelEventCode() + " to message " + response.getMessageID()); break;
                                    }
                            }
                            break;
                        }
                    default:
                        {
                            Log("Got something in DeviceResponse: " + response.ToString());
                            break;
                        }
                }
            }
            catch (Exception e)
            {
                ZBState.Log("Device response processing failed with exception: " + e.Message);
            }
        }
    }
}
