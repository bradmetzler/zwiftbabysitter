﻿namespace ZwiftBabysitter
{
    partial class LedDebugWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LedDebugWindow));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblLedsFound = new System.Windows.Forms.Label();
            this.lblZone = new System.Windows.Forms.Label();
            this.btnZ1 = new System.Windows.Forms.Button();
            this.btnZ2 = new System.Windows.Forms.Button();
            this.btnZ3 = new System.Windows.Forms.Button();
            this.btnZ6 = new System.Windows.Forms.Button();
            this.btnZ5 = new System.Windows.Forms.Button();
            this.btnZ4 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(125, 40);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(40, 40);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DragWindowMouseDownHandler);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.DragWindowMouseMoveHandler);
            // 
            // lblLedsFound
            // 
            this.lblLedsFound.BackColor = System.Drawing.Color.Transparent;
            this.lblLedsFound.ForeColor = System.Drawing.Color.Black;
            this.lblLedsFound.Location = new System.Drawing.Point(0, 81);
            this.lblLedsFound.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLedsFound.Name = "lblLedsFound";
            this.lblLedsFound.Size = new System.Drawing.Size(175, 18);
            this.lblLedsFound.TabIndex = 4;
            this.lblLedsFound.Text = "0/0 LEDs found";
            this.lblLedsFound.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // lblZone
            // 
            this.lblZone.BackColor = System.Drawing.Color.Transparent;
            this.lblZone.Font = new System.Drawing.Font("Impact", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblZone.ForeColor = System.Drawing.Color.Black;
            this.lblZone.Location = new System.Drawing.Point(0, 0);
            this.lblZone.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblZone.Name = "lblZone";
            this.lblZone.Size = new System.Drawing.Size(175, 35);
            this.lblZone.TabIndex = 5;
            this.lblZone.Text = "...";
            this.lblZone.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblZone.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DragWindowMouseDownHandler);
            this.lblZone.MouseMove += new System.Windows.Forms.MouseEventHandler(this.DragWindowMouseMoveHandler);
            // 
            // btnZ1
            // 
            this.btnZ1.Location = new System.Drawing.Point(9, 32);
            this.btnZ1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnZ1.Name = "btnZ1";
            this.btnZ1.Size = new System.Drawing.Size(35, 25);
            this.btnZ1.TabIndex = 6;
            this.btnZ1.Text = "Z1";
            this.btnZ1.UseVisualStyleBackColor = true;
            this.btnZ1.Click += new System.EventHandler(this.BtnZ1_Click);
            // 
            // btnZ2
            // 
            this.btnZ2.Location = new System.Drawing.Point(45, 32);
            this.btnZ2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnZ2.Name = "btnZ2";
            this.btnZ2.Size = new System.Drawing.Size(35, 25);
            this.btnZ2.TabIndex = 7;
            this.btnZ2.Text = "Z2";
            this.btnZ2.UseVisualStyleBackColor = true;
            this.btnZ2.Click += new System.EventHandler(this.BtnZ2_Click);
            // 
            // btnZ3
            // 
            this.btnZ3.Location = new System.Drawing.Point(81, 32);
            this.btnZ3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnZ3.Name = "btnZ3";
            this.btnZ3.Size = new System.Drawing.Size(35, 25);
            this.btnZ3.TabIndex = 8;
            this.btnZ3.Text = "Z3";
            this.btnZ3.UseVisualStyleBackColor = true;
            this.btnZ3.Click += new System.EventHandler(this.BtnZ3_Click);
            // 
            // btnZ6
            // 
            this.btnZ6.Location = new System.Drawing.Point(81, 57);
            this.btnZ6.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnZ6.Name = "btnZ6";
            this.btnZ6.Size = new System.Drawing.Size(35, 25);
            this.btnZ6.TabIndex = 11;
            this.btnZ6.Text = "Z6";
            this.btnZ6.UseVisualStyleBackColor = true;
            this.btnZ6.Click += new System.EventHandler(this.BtnZ6_Click);
            // 
            // btnZ5
            // 
            this.btnZ5.Location = new System.Drawing.Point(45, 57);
            this.btnZ5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnZ5.Name = "btnZ5";
            this.btnZ5.Size = new System.Drawing.Size(35, 25);
            this.btnZ5.TabIndex = 10;
            this.btnZ5.Text = "Z5";
            this.btnZ5.UseVisualStyleBackColor = true;
            this.btnZ5.Click += new System.EventHandler(this.BtnZ5_Click);
            // 
            // btnZ4
            // 
            this.btnZ4.Location = new System.Drawing.Point(9, 57);
            this.btnZ4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnZ4.Name = "btnZ4";
            this.btnZ4.Size = new System.Drawing.Size(35, 25);
            this.btnZ4.TabIndex = 9;
            this.btnZ4.Text = "Z4";
            this.btnZ4.UseVisualStyleBackColor = true;
            this.btnZ4.Click += new System.EventHandler(this.BtnZ4_Click);
            // 
            // LedDebugWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(175, 100);
            this.Controls.Add(this.btnZ6);
            this.Controls.Add(this.btnZ5);
            this.Controls.Add(this.btnZ4);
            this.Controls.Add(this.btnZ3);
            this.Controls.Add(this.btnZ2);
            this.Controls.Add(this.btnZ1);
            this.Controls.Add(this.lblZone);
            this.Controls.Add(this.lblLedsFound);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "LedDebugWindow";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "LedDebugWindow";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.LedDebugWindow_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DragWindowMouseDownHandler);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.DragWindowMouseMoveHandler);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblLedsFound;
        private System.Windows.Forms.Label lblZone;
        private System.Windows.Forms.Button btnZ1;
        private System.Windows.Forms.Button btnZ2;
        private System.Windows.Forms.Button btnZ3;
        private System.Windows.Forms.Button btnZ6;
        private System.Windows.Forms.Button btnZ5;
        private System.Windows.Forms.Button btnZ4;
    }
}