﻿namespace ZwiftBabysitter
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.logBox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.statusLabelVirtualhere = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.statusLabelTimestamp = new System.Windows.Forms.Label();
            this.statusLabelZwift = new System.Windows.Forms.Label();
            this.statusLabelLauncher = new System.Windows.Forms.Label();
            this.statusLabelBorderless = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.killSwitchKillBtn = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.killSwitchSafety2Btn = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.killSwitchSafety1Btn = new System.Windows.Forms.Button();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripTools = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripToolsSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripHelp = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripHelpAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripHelpLedLegend = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // logBox
            // 
            this.logBox.BackColor = System.Drawing.Color.White;
            this.logBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.logBox.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.logBox.Location = new System.Drawing.Point(7, 22);
            this.logBox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.logBox.Multiline = true;
            this.logBox.Name = "logBox";
            this.logBox.ReadOnly = true;
            this.logBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.logBox.Size = new System.Drawing.Size(679, 219);
            this.logBox.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.logBox);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.groupBox1.Location = new System.Drawing.Point(14, 219);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox1.Size = new System.Drawing.Size(693, 248);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Log";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.statusLabelVirtualhere);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.statusLabelTimestamp);
            this.groupBox2.Controls.Add(this.statusLabelZwift);
            this.groupBox2.Controls.Add(this.statusLabelLauncher);
            this.groupBox2.Controls.Add(this.statusLabelBorderless);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.groupBox2.Location = new System.Drawing.Point(14, 32);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox2.Size = new System.Drawing.Size(509, 180);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Status";
            // 
            // statusLabelVirtualhere
            // 
            this.statusLabelVirtualhere.AutoSize = true;
            this.statusLabelVirtualhere.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.statusLabelVirtualhere.ForeColor = System.Drawing.Color.Silver;
            this.statusLabelVirtualhere.Location = new System.Drawing.Point(288, 18);
            this.statusLabelVirtualhere.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.statusLabelVirtualhere.Name = "statusLabelVirtualhere";
            this.statusLabelVirtualhere.Size = new System.Drawing.Size(101, 25);
            this.statusLabelVirtualhere.TabIndex = 9;
            this.statusLabelVirtualhere.Text = "Unknown";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label5.Location = new System.Drawing.Point(137, 105);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(119, 25);
            this.label5.TabIndex = 8;
            this.label5.Text = "Zwift Status:";
            // 
            // statusLabelTimestamp
            // 
            this.statusLabelTimestamp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.statusLabelTimestamp.ForeColor = System.Drawing.Color.Silver;
            this.statusLabelTimestamp.Location = new System.Drawing.Point(7, 140);
            this.statusLabelTimestamp.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.statusLabelTimestamp.Name = "statusLabelTimestamp";
            this.statusLabelTimestamp.Size = new System.Drawing.Size(495, 23);
            this.statusLabelTimestamp.TabIndex = 7;
            this.statusLabelTimestamp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // statusLabelZwift
            // 
            this.statusLabelZwift.AutoSize = true;
            this.statusLabelZwift.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.statusLabelZwift.ForeColor = System.Drawing.Color.Silver;
            this.statusLabelZwift.Location = new System.Drawing.Point(288, 105);
            this.statusLabelZwift.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.statusLabelZwift.Name = "statusLabelZwift";
            this.statusLabelZwift.Size = new System.Drawing.Size(101, 25);
            this.statusLabelZwift.TabIndex = 5;
            this.statusLabelZwift.Text = "Unknown";
            // 
            // statusLabelLauncher
            // 
            this.statusLabelLauncher.AutoSize = true;
            this.statusLabelLauncher.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.statusLabelLauncher.ForeColor = System.Drawing.Color.Silver;
            this.statusLabelLauncher.Location = new System.Drawing.Point(288, 76);
            this.statusLabelLauncher.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.statusLabelLauncher.Name = "statusLabelLauncher";
            this.statusLabelLauncher.Size = new System.Drawing.Size(101, 25);
            this.statusLabelLauncher.TabIndex = 4;
            this.statusLabelLauncher.Text = "Unknown";
            // 
            // statusLabelBorderless
            // 
            this.statusLabelBorderless.AutoSize = true;
            this.statusLabelBorderless.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.statusLabelBorderless.ForeColor = System.Drawing.Color.Silver;
            this.statusLabelBorderless.Location = new System.Drawing.Point(288, 47);
            this.statusLabelBorderless.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.statusLabelBorderless.Name = "statusLabelBorderless";
            this.statusLabelBorderless.Size = new System.Drawing.Size(101, 25);
            this.statusLabelBorderless.TabIndex = 3;
            this.statusLabelBorderless.Text = "Unknown";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(95, 76);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(161, 25);
            this.label3.TabIndex = 2;
            this.label3.Text = "Launcher Status:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(80, 18);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(176, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "VirtualHere Status:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(84, 47);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(172, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Borderless Status:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.killSwitchKillBtn);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.killSwitchSafety2Btn);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.killSwitchSafety1Btn);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.groupBox3.Location = new System.Drawing.Point(530, 32);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox3.Size = new System.Drawing.Size(177, 180);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Kill Switch";
            // 
            // killSwitchKillBtn
            // 
            this.killSwitchKillBtn.Enabled = false;
            this.killSwitchKillBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.killSwitchKillBtn.Location = new System.Drawing.Point(40, 117);
            this.killSwitchKillBtn.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.killSwitchKillBtn.Name = "killSwitchKillBtn";
            this.killSwitchKillBtn.Size = new System.Drawing.Size(96, 27);
            this.killSwitchKillBtn.TabIndex = 4;
            this.killSwitchKillBtn.Text = "Kill It!";
            this.killSwitchKillBtn.UseVisualStyleBackColor = true;
            this.killSwitchKillBtn.Click += new System.EventHandler(this.KillSwitchKillBtn_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label7.Location = new System.Drawing.Point(76, 97);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(20, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Kill";
            // 
            // killSwitchSafety2Btn
            // 
            this.killSwitchSafety2Btn.Enabled = false;
            this.killSwitchSafety2Btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.killSwitchSafety2Btn.Location = new System.Drawing.Point(98, 48);
            this.killSwitchSafety2Btn.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.killSwitchSafety2Btn.Name = "killSwitchSafety2Btn";
            this.killSwitchSafety2Btn.Size = new System.Drawing.Size(37, 27);
            this.killSwitchSafety2Btn.TabIndex = 2;
            this.killSwitchSafety2Btn.Text = "2";
            this.killSwitchSafety2Btn.UseVisualStyleBackColor = true;
            this.killSwitchSafety2Btn.Click += new System.EventHandler(this.KillSwitchSafety2Btn_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label6.Location = new System.Drawing.Point(62, 29);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Safeties";
            // 
            // killSwitchSafety1Btn
            // 
            this.killSwitchSafety1Btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.killSwitchSafety1Btn.Location = new System.Drawing.Point(40, 48);
            this.killSwitchSafety1Btn.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.killSwitchSafety1Btn.Name = "killSwitchSafety1Btn";
            this.killSwitchSafety1Btn.Size = new System.Drawing.Size(37, 27);
            this.killSwitchSafety1Btn.TabIndex = 0;
            this.killSwitchSafety1Btn.Text = "1";
            this.killSwitchSafety1Btn.UseVisualStyleBackColor = true;
            this.killSwitchSafety1Btn.Click += new System.EventHandler(this.KillSwitchSafety1Btn_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.AllowMerge = false;
            this.toolStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip1.CanOverflow = false;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTools,
            this.toolStripHelp});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.ShowItemToolTips = false;
            this.toolStrip1.Size = new System.Drawing.Size(721, 25);
            this.toolStrip1.TabIndex = 5;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripTools
            // 
            this.toolStripTools.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripTools.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripToolsSettings});
            this.toolStripTools.Image = ((System.Drawing.Image)(resources.GetObject("toolStripTools.Image")));
            this.toolStripTools.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripTools.Name = "toolStripTools";
            this.toolStripTools.ShowDropDownArrow = false;
            this.toolStripTools.Size = new System.Drawing.Size(38, 22);
            this.toolStripTools.Text = "Tools";
            // 
            // toolStripToolsSettings
            // 
            this.toolStripToolsSettings.Image = ((System.Drawing.Image)(resources.GetObject("toolStripToolsSettings.Image")));
            this.toolStripToolsSettings.Name = "toolStripToolsSettings";
            this.toolStripToolsSettings.Size = new System.Drawing.Size(180, 22);
            this.toolStripToolsSettings.Text = "Settings...";
            this.toolStripToolsSettings.Click += new System.EventHandler(this.ToolStripToolsSettings_Click);
            // 
            // toolStripHelp
            // 
            this.toolStripHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripHelpAbout,
            this.toolStripHelpLedLegend});
            this.toolStripHelp.Image = ((System.Drawing.Image)(resources.GetObject("toolStripHelp.Image")));
            this.toolStripHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripHelp.Name = "toolStripHelp";
            this.toolStripHelp.ShowDropDownArrow = false;
            this.toolStripHelp.Size = new System.Drawing.Size(36, 22);
            this.toolStripHelp.Text = "Help";
            // 
            // toolStripHelpAbout
            // 
            this.toolStripHelpAbout.Image = ((System.Drawing.Image)(resources.GetObject("toolStripHelpAbout.Image")));
            this.toolStripHelpAbout.Name = "toolStripHelpAbout";
            this.toolStripHelpAbout.Size = new System.Drawing.Size(201, 22);
            this.toolStripHelpAbout.Text = "About Zwift Babysitter...";
            this.toolStripHelpAbout.Click += new System.EventHandler(this.ToolStripHelpAbout_Click);
            // 
            // toolStripHelpLedLegend
            // 
            this.toolStripHelpLedLegend.Image = ((System.Drawing.Image)(resources.GetObject("toolStripHelpLedLegend.Image")));
            this.toolStripHelpLedLegend.Name = "toolStripHelpLedLegend";
            this.toolStripHelpLedLegend.Size = new System.Drawing.Size(201, 22);
            this.toolStripHelpLedLegend.Text = "LED Color Legend...";
            this.toolStripHelpLedLegend.Click += new System.EventHandler(this.ToolStripHelpLedLegend_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(721, 480);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainWindow";
            this.Text = "Zwift Babysitter";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox logBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label statusLabelTimestamp;
        private System.Windows.Forms.Label statusLabelZwift;
        private System.Windows.Forms.Label statusLabelLauncher;
        private System.Windows.Forms.Label statusLabelBorderless;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label statusLabelVirtualhere;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button killSwitchKillBtn;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button killSwitchSafety2Btn;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button killSwitchSafety1Btn;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripTools;
        private System.Windows.Forms.ToolStripMenuItem toolStripToolsSettings;
        private System.Windows.Forms.ToolStripDropDownButton toolStripHelp;
        private System.Windows.Forms.ToolStripMenuItem toolStripHelpAbout;
        private System.Windows.Forms.ToolStripMenuItem toolStripHelpLedLegend;
    }
}

