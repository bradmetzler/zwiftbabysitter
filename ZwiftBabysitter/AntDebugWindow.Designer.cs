﻿namespace ZwiftBabysitter
{
    partial class AntDebugWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AntDebugWindow));
            this.lblWatts = new System.Windows.Forms.Label();
            this.lblPowerMeter = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblLowBattery = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblWatts
            // 
            this.lblWatts.BackColor = System.Drawing.Color.Transparent;
            this.lblWatts.Font = new System.Drawing.Font("Impact", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblWatts.ForeColor = System.Drawing.Color.Black;
            this.lblWatts.Location = new System.Drawing.Point(10, 12);
            this.lblWatts.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblWatts.Name = "lblWatts";
            this.lblWatts.Size = new System.Drawing.Size(180, 54);
            this.lblWatts.TabIndex = 0;
            this.lblWatts.Text = "0w";
            this.lblWatts.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblWatts.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DragWindowMouseDownHandler);
            this.lblWatts.MouseMove += new System.Windows.Forms.MouseEventHandler(this.DragWindowMouseMoveHandler);
            // 
            // lblPowerMeter
            // 
            this.lblPowerMeter.BackColor = System.Drawing.Color.Transparent;
            this.lblPowerMeter.ForeColor = System.Drawing.Color.Black;
            this.lblPowerMeter.Location = new System.Drawing.Point(0, 84);
            this.lblPowerMeter.Margin = new System.Windows.Forms.Padding(0);
            this.lblPowerMeter.Name = "lblPowerMeter";
            this.lblPowerMeter.Size = new System.Drawing.Size(200, 14);
            this.lblPowerMeter.TabIndex = 1;
            this.lblPowerMeter.Text = "0 (?)";
            this.lblPowerMeter.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.lblPowerMeter.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DragWindowMouseDownHandler);
            this.lblPowerMeter.MouseMove += new System.Windows.Forms.MouseEventHandler(this.DragWindowMouseMoveHandler);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(150, 40);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(40, 40);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DragWindowMouseDownHandler);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.DragWindowMouseMoveHandler);
            // 
            // lblLowBattery
            // 
            this.lblLowBattery.BackColor = System.Drawing.Color.DeepPink;
            this.lblLowBattery.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.lblLowBattery.ForeColor = System.Drawing.Color.Black;
            this.lblLowBattery.Location = new System.Drawing.Point(0, 66);
            this.lblLowBattery.Margin = new System.Windows.Forms.Padding(0);
            this.lblLowBattery.Name = "lblLowBattery";
            this.lblLowBattery.Size = new System.Drawing.Size(149, 14);
            this.lblLowBattery.TabIndex = 3;
            this.lblLowBattery.Text = "LOW BATTERY";
            this.lblLowBattery.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblLowBattery.Visible = false;
            // 
            // AntDebugWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(200, 100);
            this.Controls.Add(this.lblPowerMeter);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblWatts);
            this.Controls.Add(this.lblLowBattery);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "AntDebugWindow";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "ANT+ Debug";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.AntDebugWindow_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DragWindowMouseDownHandler);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.DragWindowMouseMoveHandler);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblWatts;
        private System.Windows.Forms.Label lblPowerMeter;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblLowBattery;
    }
}