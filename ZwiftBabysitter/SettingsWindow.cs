﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace ZwiftBabysitter
{
    public partial class SettingsWindow : Form
    {
        private AntPowerMeterDevice selectedPowerMeter;
        private List<LedLight> selectedLedBulbs;
        private List<LedLight> selectedLedStrips;

        public SettingsWindow()
        {
            InitializeComponent();
            
            //General Settings tab
            numTimerInterval.Value = (ZBConfig.mainTimerInterval < 1000 || !ZBConfig.configIsValid) ? 30000 : ZBConfig.mainTimerInterval;
            
            cbVirtualHereEnabled.Checked = ZBConfig.virtualHereEnabled;
            tbVirtualHereLocation.Text = ZBConfig.virtualHereExe;
            tbVirtualHereLocation.Enabled = ZBConfig.virtualHereEnabled;
            tbVirtualHereArgs.Text = ZBConfig.configIsValid ? ZBConfig.virtualHereArgs : "-g -e";
            tbVirtualHereArgs.Enabled = ZBConfig.virtualHereEnabled;
            btnVirtualHereBrowse.Enabled = ZBConfig.virtualHereEnabled;

            cbBorderlessGamingEnabled.Checked = ZBConfig.borderlessGamingEnabled;
            tbBorderlessGamingLocation.Text = ZBConfig.borderlessGamingExe;
            tbBorderlessGamingLocation.Enabled = ZBConfig.borderlessGamingEnabled;
            tbBorderlessGamingArgs.Text = ZBConfig.borderlessGamingArgs;
            tbBorderlessGamingArgs.Enabled = ZBConfig.borderlessGamingEnabled;
            btnBorderlessGamingBrowse.Enabled = ZBConfig.borderlessGamingEnabled;

            tbZwiftLauncherLocation.Text = ZBConfig.zwiftLauncherExe;
            tbZwiftLauncherArgs.Text = ZBConfig.zwiftLauncherArgs;

            tbZwiftAppLocation.Text = ZBConfig.zwiftExe;


            //ANT+/LED Settings tab
            cbLedsEnabled.Checked = ZBConfig.ledsEnabled;
            
            cbAntDebugLogEnabled.Checked = ZBConfig.antDebugLogEnabled;
            cbAntDebugLogEnabled.Enabled = ZBConfig.ledsEnabled;

            cbAntDebugWindowEnabled.Checked = ZBConfig.antDebugWindowEnabled;
            cbAntDebugWindowEnabled.Enabled = ZBConfig.ledsEnabled;

            numUsbId.Value = ZBConfig.antUsbId;
            numUsbId.Enabled = ZBConfig.ledsEnabled;

            selectedPowerMeter = new AntPowerMeterDevice(ZBConfig.powerMeter.deviceNumber, ZBConfig.powerMeter.transmissionTypeId, ZBConfig.powerMeter.manufacturerId);
            tbPowerMeterDisplay.Text = selectedPowerMeter.GetPrettyFormat();
            btnPowerMeterChoose.Enabled = ZBConfig.ledsEnabled;

            numFtpWatts.Value = ZBConfig.ftpWatts;
            numFtpWatts.Enabled = ZBConfig.ledsEnabled;

            cbLedDebugLogEnabled.Checked = ZBConfig.ledDebugLogEnabled;
            cbLedDebugLogEnabled.Enabled = ZBConfig.ledsEnabled;

            cbLedDebugWindowEnabled.Checked = ZBConfig.ledDebugWindowEnabled;
            cbLedDebugWindowEnabled.Enabled = ZBConfig.ledsEnabled;

            selectedLedBulbs = new List<LedLight>(ZBConfig.ledBulbs);
            selectedLedStrips = new List<LedLight>(ZBConfig.ledStrips);
            UpdateSelectedLightsText();
            btnLedChoose.Enabled = ZBConfig.ledsEnabled;
        }

        private void BtnVirtualHereBrowse_Click(object sender, EventArgs e)
        {
            String dir = String.Empty;
            try
            {
                dir = System.IO.Path.GetDirectoryName(tbVirtualHereLocation.Text);
            }
            catch (Exception) { }

            OpenFileDialog ofd = new OpenFileDialog
            {
                InitialDirectory = dir,
                Title = "Locate vhui64.exe",

                CheckFileExists = true,
                CheckPathExists = true,

                DefaultExt = "exe",
                Filter = "Executables (*.exe)|*.exe",
                RestoreDirectory = true,
                ShowReadOnly = false
            };

            if(ofd.ShowDialog() == DialogResult.OK)
            {
                tbVirtualHereLocation.Text = ofd.FileName;
            }
        }

        private void BtnBorderlessGamingBrowse_Click(object sender, EventArgs e)
        {
            String dir = String.Empty;
            try
            {
                dir = System.IO.Path.GetDirectoryName(tbBorderlessGamingLocation.Text);
            }
            catch(Exception) { }

            OpenFileDialog ofd = new OpenFileDialog
            {
                InitialDirectory = dir,
                Title = "Locate BorderlessGaming.exe",

                CheckFileExists = true,
                CheckPathExists = true,

                DefaultExt = "exe",
                Filter = "Executables (*.exe)|*.exe",
                RestoreDirectory = true,
                ShowReadOnly = false
            };

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                tbBorderlessGamingLocation.Text = ofd.FileName;
            }
        }

        private void BtnZwiftLauncherBrowse_Click(object sender, EventArgs e)
        {
            String dir = String.Empty;
            try
            {
                dir = System.IO.Path.GetDirectoryName(tbZwiftLauncherLocation.Text);
            }
            catch (Exception) { }

            OpenFileDialog ofd = new OpenFileDialog
            {
                InitialDirectory = dir,
                Title = "Locate ZwiftLauncher.exe",

                CheckFileExists = true,
                CheckPathExists = true,

                DefaultExt = "exe",
                Filter = "Executables (*.exe)|*.exe",
                RestoreDirectory = true,
                ShowReadOnly = false
            };

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                tbZwiftLauncherLocation.Text = ofd.FileName;
            }
        }

        private void BtnZwiftAppBrowse_Click(object sender, EventArgs e)
        {
            String dir = String.Empty;
            try
            {
                dir = System.IO.Path.GetDirectoryName(tbZwiftAppLocation.Text);
            }
            catch(Exception) { }

            OpenFileDialog ofd = new OpenFileDialog
            {
                InitialDirectory = dir,
                Title = "Locate ZwiftApp.exe",

                CheckFileExists = true,
                CheckPathExists = true,

                DefaultExt = "exe",
                Filter = "Executables (*.exe)|*.exe",
                RestoreDirectory = true,
                ShowReadOnly = false
            };

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                tbZwiftAppLocation.Text = ofd.FileName;
            }
        }

        private void CbVirtualHereEnabled_CheckedChanged(object sender, EventArgs e)
        {
            tbVirtualHereLocation.Enabled = cbVirtualHereEnabled.Checked;
            tbVirtualHereArgs.Enabled = cbVirtualHereEnabled.Checked;
            btnVirtualHereBrowse.Enabled = cbVirtualHereEnabled.Checked;
        }

        private void CbBorderlessGamingEnabled_CheckedChanged(object sender, EventArgs e)
        {
            tbBorderlessGamingLocation.Enabled = cbBorderlessGamingEnabled.Checked;
            tbBorderlessGamingArgs.Enabled = cbBorderlessGamingEnabled.Checked;
            btnBorderlessGamingBrowse.Enabled = cbBorderlessGamingEnabled.Checked;
        }

        private void CbLedsEnabled_CheckedChanged(object sender, EventArgs e)
        {
            cbAntDebugLogEnabled.Enabled = cbLedsEnabled.Checked;
            cbAntDebugWindowEnabled.Enabled = cbLedsEnabled.Checked;
            numUsbId.Enabled = cbLedsEnabled.Checked;
            btnPowerMeterChoose.Enabled = cbLedsEnabled.Checked;
            numFtpWatts.Enabled = cbLedsEnabled.Checked;
            cbLedDebugLogEnabled.Enabled = cbLedsEnabled.Checked;
            cbLedDebugWindowEnabled.Enabled = cbLedsEnabled.Checked;
            btnLedChoose.Enabled = cbLedsEnabled.Checked;
        }

        private void BtnPowerMeterChoose_Click(object sender, EventArgs e)
        {
            ZBState.Log("Opening Choose Power Meter window");
            ChoosePowerMeterWindow powerMeterWindow = new ChoosePowerMeterWindow(selectedPowerMeter);
            powerMeterWindow.FormClosed += (s, ea) => PowerMeterWindowClosed(powerMeterWindow.DialogResult);
            powerMeterWindow.ShowDialog();
        }

        private void BtnLedChoose_Click(object sender, EventArgs e)
        {
            ZBState.Log("Opening Choose Lights window");
            ChooseLightsWindow lightsWindow = new ChooseLightsWindow(selectedLedBulbs, selectedLedStrips);
            lightsWindow.FormClosed += (s, ea) => LightsWindowClosed(lightsWindow.DialogResult);
            lightsWindow.ShowDialog();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (ValidateConfig())
            {
                WriteConfig();
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void PowerMeterWindowClosed(DialogResult result)
        {
            ZBState.Log("Choose Power Meter window closed: " + (result == DialogResult.OK ? "Save" : "Cancel"));

            tbPowerMeterDisplay.Text = selectedPowerMeter.GetPrettyFormat();
        }

        private void LightsWindowClosed(DialogResult result)
        {
            ZBState.Log("Choose Lights window closed: " + (result == DialogResult.OK ? "Save" : "Cancel"));

            UpdateSelectedLightsText();
        }

        private void UpdateSelectedLightsText()
        {
            tbLedBulbDisplay.Text = BuildSelectedLightsString(selectedLedBulbs, "Bulb", "Bulbs");
            tbLedStripDisplay.Text = BuildSelectedLightsString(selectedLedStrips, "Strip", "Strips");
        }

        private String BuildSelectedLightsString(List<LedLight> selectedLights, String singular, String plural)
        {
            if (selectedLights.Count == 0)
                return "(none)";

            StringBuilder x = new StringBuilder();

            x.Append(selectedLights.Count);
            x.Append(' ');
            x.Append(selectedLights.Count == 1 ? singular : plural);
            x.Append(" (");

            foreach (LedLight light in selectedLights)
            {
                x.Append(light.GetPrettyFormat());
                x.Append(", ");
            }

            x.Remove(x.Length - 2, 2);
            x.Append(')');

            return x.ToString();
        }

        private bool ValidateConfig()
        {
            //Validate and pop dialogs if invalid
            bool isValid = true;

            if(cbVirtualHereEnabled.Checked && !System.IO.File.Exists(tbVirtualHereLocation.Text))
            {
                MessageBox.Show("VirtualHere not found where specified!", "Invalid Config", MessageBoxButtons.OK, MessageBoxIcon.Error);
                isValid = false;
            }

            if (cbBorderlessGamingEnabled.Checked && !System.IO.File.Exists(tbBorderlessGamingLocation.Text))
            {
                MessageBox.Show("BorderlessGaming not found where specified!", "Invalid Config", MessageBoxButtons.OK, MessageBoxIcon.Error);
                isValid = false;
            }

            if (!System.IO.File.Exists(tbZwiftLauncherLocation.Text))
            {
                MessageBox.Show("ZwiftLauncher not found where specified!", "Invalid Config", MessageBoxButtons.OK, MessageBoxIcon.Error);
                isValid = false;
            }

            if (!System.IO.File.Exists(tbZwiftAppLocation.Text))
            {
                MessageBox.Show("ZwiftApp not found where specified!", "Invalid Config", MessageBoxButtons.OK, MessageBoxIcon.Error);
                isValid = false;
            }

            return isValid;
        }

        private void WriteConfig()
        {
            //Save new General Settings values to live config
            ZBConfig.mainTimerInterval = (int)numTimerInterval.Value;
            ZBConfig.virtualHereEnabled = cbVirtualHereEnabled.Checked;
            ZBConfig.virtualHereExe = tbVirtualHereLocation.Text;
            ZBConfig.virtualHereArgs = tbVirtualHereArgs.Text;
            ZBConfig.borderlessGamingEnabled = cbBorderlessGamingEnabled.Checked;
            ZBConfig.borderlessGamingExe = tbBorderlessGamingLocation.Text;
            ZBConfig.borderlessGamingArgs = tbBorderlessGamingArgs.Text;
            ZBConfig.zwiftLauncherExe = tbZwiftLauncherLocation.Text;
            ZBConfig.zwiftLauncherArgs = tbZwiftLauncherArgs.Text;
            ZBConfig.zwiftExe = tbZwiftAppLocation.Text;

            //Save new ANT+/LED Settings values to live config
            ZBConfig.ledsEnabled = cbLedsEnabled.Checked;
            ZBConfig.antDebugLogEnabled = cbAntDebugLogEnabled.Checked;
            ZBConfig.antDebugWindowEnabled = cbAntDebugWindowEnabled.Checked;
            ZBConfig.antUsbId = (int)numUsbId.Value;
            ZBConfig.powerMeter.deviceNumber = selectedPowerMeter.deviceNumber;
            ZBConfig.powerMeter.transmissionTypeId = selectedPowerMeter.transmissionTypeId;
            ZBConfig.powerMeter.manufacturerId = selectedPowerMeter.manufacturerId;
            ZBConfig.ftpWatts = (int)numFtpWatts.Value;
            ZBConfig.ledDebugLogEnabled = cbLedDebugLogEnabled.Checked;
            ZBConfig.ledDebugWindowEnabled = cbLedDebugWindowEnabled.Checked;
            ZBConfig.ledBulbs.Clear();
            ZBConfig.ledStrips.Clear();
            ZBConfig.ledBulbs.AddRange(selectedLedBulbs);
            ZBConfig.ledStrips.AddRange(selectedLedStrips);

            //Delegate writing the config to ZBSConfig
            ZBConfig.WriteConfig();
        }

        private void LblUsbIdInfo_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MessageBox.Show("Zwift typically connects to the first ANT+ stick with the lowest ID (0). Leave this setting to -1 and Zwift Babysitter will connect to the ANT+ stick with the highest ID in an attempt to avoid conflicting with Zwift.\n\nHowever, in some cases, this does not work. This setting allows you to manually specify which ANT+ USB stick Zwift Babysitter will use.\n\nIt may take some trial and error to arrive at a value that plays nicely with Zwift.", "ANT+ USB ID", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
