﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace ZwiftBabysitter
{
    public partial class AntDebugWindow : DebugWindowBase
    {   
        public AntDebugWindow()
        {
            InitializeComponent();

            lblPowerMeter.Text = ZBConfig.powerMeter.GetPrettyFormat();

            Screen screen = Screen.FromControl(this);
            this.Location = new Point(screen.Bounds.Left + 50, screen.Bounds.Bottom - this.Height - 50);
        }

        public void UpdatePowerData(int watts, byte instantaneousZone)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new Action<int, byte>(UpdatePowerData), new object[]{watts, instantaneousZone});
                return;
            }

            //Update wattage display
            if (watts > -1)
                lblWatts.Text = watts + "w";
            else
                lblWatts.Text = "...";

            lblPowerMeter.Text = ZBConfig.powerMeter.GetPrettyFormat();

            //Update main background. This is color-coded based on zone, instantaneously (no smoothing) to match the wattage display
            if (instantaneousZone == 1)
                this.BackColor = zone1Color;
            else if (instantaneousZone == 2)
                this.BackColor = zone2Color;
            else if (instantaneousZone == 3)
                this.BackColor = zone3Color;
            else if (instantaneousZone == 4)
                this.BackColor = zone4Color;
            else if (instantaneousZone == 5)
                this.BackColor = zone5Color;
            else if (instantaneousZone == 6)
                this.BackColor = zone6Color;
            else
                this.BackColor = defaultColor;

            TransparetBackground(lblWatts);
            TransparetBackground(lblPowerMeter);
        }

        public void UpdateBatteryLow(bool batteryLow)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new Action<bool>(UpdateBatteryLow), new object[] { batteryLow });
                return;
            }

            lblLowBattery.Visible = batteryLow;
        }

        private void AntDebugWindow_Load(object sender, EventArgs e)
        {
            UpdatePowerData(-1, 0);
        }

        protected override void DragWindowMouseDownHandler(object sender, MouseEventArgs e)
        {
            base.DragWindowMouseDownHandler(sender, e);
        }

        protected override void DragWindowMouseMoveHandler(object sender, MouseEventArgs e)
        {
            base.DragWindowMouseMoveHandler(sender, e);
        }
    }
}
