﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace ZwiftBabysitter
{
    public partial class LedDebugWindow : DebugWindowBase
    {
        private Action<byte> sendLedUpdateCallback;
        private byte currentZone = 0;

        public LedDebugWindow(Action<byte> sendLedUpdateCallback)
        {
            InitializeComponent();

            this.sendLedUpdateCallback = sendLedUpdateCallback;

            Screen screen = Screen.FromControl(this);
            this.Location = new Point(screen.Bounds.Left + 250, screen.Bounds.Bottom - this.Height - 50);
        }

        public void UpdateLedZone(byte ledZone)
        {
            currentZone = ledZone;

            //Update Zone display
            lblZone.Text = "Z" + ledZone;

            //Update main background. This is color-coded based on zone, with smoothing applied from AntLedController
            if (ledZone == 1)
                this.BackColor = zone1Color;
            else if (ledZone == 2)
                this.BackColor = zone2Color;
            else if (ledZone == 3)
                this.BackColor = zone3Color;
            else if (ledZone == 4)
                this.BackColor = zone4Color;
            else if (ledZone == 5)
                this.BackColor = zone5Color;
            else if (ledZone == 6)
                this.BackColor = zone6Color;
            else
                this.BackColor = defaultColor;

            TransparetBackground(lblZone);
            TransparetBackground(lblLedsFound);
        }

        public void UpdateConnectedLeds(int connectedLeds)
        {
            lblLedsFound.Text = connectedLeds + "/" + (ZBConfig.ledBulbs.Count + ZBConfig.ledStrips.Count) + " LEDs found";
        }

        private void TestLedZone(byte testZone)
        {
            sendLedUpdateCallback(testZone);
        }

        private void BtnZ1_Click(object sender, System.EventArgs e)
        {
            TestLedZone(currentZone == 1 ? (byte)0 : (byte)1);
        }

        private void BtnZ2_Click(object sender, System.EventArgs e)
        {
            TestLedZone(currentZone == 2 ? (byte)0 : (byte)2);
        }

        private void BtnZ3_Click(object sender, System.EventArgs e)
        {
            TestLedZone(currentZone == 3 ? (byte)0 : (byte)3);
        }

        private void BtnZ4_Click(object sender, System.EventArgs e)
        {
            TestLedZone(currentZone == 4 ? (byte)0 : (byte)4);
        }

        private void BtnZ5_Click(object sender, System.EventArgs e)
        {
            TestLedZone(currentZone == 5 ? (byte)0 : (byte)5);
        }

        private void BtnZ6_Click(object sender, System.EventArgs e)
        {
            TestLedZone(currentZone == 6 ? (byte)0 : (byte)6);
        }

        private void LedDebugWindow_Load(object sender, System.EventArgs e)
        {
            UpdateLedZone(0);
            UpdateConnectedLeds(0);
        }

        protected override void DragWindowMouseDownHandler(object sender, MouseEventArgs e)
        {
            base.DragWindowMouseDownHandler(sender, e);
        }

        protected override void DragWindowMouseMoveHandler(object sender, MouseEventArgs e)
        {
            base.DragWindowMouseMoveHandler(sender, e);
        }
    }
}
