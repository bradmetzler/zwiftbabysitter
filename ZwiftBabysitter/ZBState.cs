﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;

namespace ZwiftBabysitter
{
    internal class ZBState
    {
        private static readonly String logFileName = "ZwiftBabysitter.log";
        private static TextWriter logFile;
        private static readonly ConcurrentQueue<String> logBuffer = new ConcurrentQueue<String>();

        //Class for reading ANT+ power data and controlling LEDs accordingly
        public static AntLedController antLedController = null;

        //Main Window status text stuff
        public static Process virtualHereProcess = null;
        public static Process borderlessGamingProcess = null;
        public static Process zwiftLauncherProcess = null;
        public static Process zwiftProcess = null;
        public static bool zwiftDetected = false;
        public static bool zwiftDeadOnLastCheck = false;

        static ZBState()
        {
            StreamWriter logWriter = new StreamWriter(logFileName, false);
            logWriter.AutoFlush = true;

            logFile = TextWriter.Synchronized(logWriter);
            
        }

        public static void CloseLogFile()
        {
            if(logFile != null)
            {
                logFile.Flush();
                logFile.Close();
            }
        }

        public static long CurrentTimeMillis()
        {
            return DateTimeOffset.Now.ToUnixTimeMilliseconds();
        }

        public static String Timestamp()
        {
            return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }

        public static void Log(String message)
        {
            int threadId = Thread.CurrentThread.ManagedThreadId;

            StringBuilder log = new StringBuilder();
            log.Append(Timestamp());
            if (threadId < 10)
                log.Append(' ');
            log.Append(" (");
            log.Append(threadId);
            log.Append(") - ");
            log.Append(message);

            String formattedLog = log.ToString();
            
            logBuffer.Enqueue(formattedLog);
            logFile.WriteLine(formattedLog);
        }

        public static String[] FetchLogs()
        {
            int numLogs = logBuffer.Count;

            String[] logs = new String[numLogs];
            for (int i = 0; i < numLogs; i++)
            {
                logBuffer.TryDequeue(out logs[i]);
            }

            return logs;
        }
    }
}
