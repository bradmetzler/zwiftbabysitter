﻿using ANT_Managed_Library;
using System;
using System.Timers;

namespace ZwiftBabysitter
{
    internal class AntPowerMeterListener : AntBase
    {
        private const double pi128 = 402.1238596594935;     //128 * PI (used in torque -> power calculations)
        private const int maxIgnoredDuplicates = 8;         //Roughly 2 seconds worth of messages before we would recognize it as legitimately 0 watts

        private Action<int> powerUpdateCallback;
        private Action<bool> batteryStateCallback;
        private Action<bool> connectionStatusCallback;
        private Action antErrorCallback;
        private bool connected = false;
        private bool readingBasicInfo = true;

        private int previousPeriod = 0;
        private int previousAccumTorque = 0;
        private int duplicatesReceived = 0;

        private Timer connectionResetTimer;
        private int connectionResetDelay = 2500;            //Milliseconds to wait before re-opening our ANT+ channel if it gets disconnected (typically happens if the power meter has turned off due to inactivity)

        public AntPowerMeterListener(Action<int> powerUpdateCallback, Action<bool> batteryStateCallback, Action<bool> connectionStatusCallback, Action antErrorCallback)
        {
            this.powerUpdateCallback = powerUpdateCallback;
            this.batteryStateCallback = batteryStateCallback;
            this.connectionStatusCallback = connectionStatusCallback;
            this.antErrorCallback = antErrorCallback;
        }

        private void OpenChannel()
        {
            OpenAntChannel(0, (ushort)ZBConfig.powerMeter.deviceNumber, (byte)ZBConfig.powerMeter.transmissionTypeId);
        }

        public void StartListening()
        {
            ZBState.Log("Starting ANT+ Listener for " + ZBConfig.powerMeter.GetPrettyFormat() + "...");

            try
            {
                InitAntDevice();
                channels = new ANT_Channel[1];
                OpenChannel();
            }
            catch (Exception ex)
            {
                ZBState.Log("ANT+ failed with exception: \n" + ex.Message);
                StopListening();
                if (antErrorCallback != null)
                    antErrorCallback();
            }
        }

        public void StopListening()
        {
            if (isRunning)
            {
                ZBState.Log("Stopping ANT+ Listener...");
                CleanUpANT();
            }
        }

        protected override void ReceiveAntPageData(byte channelNum, ANT_ChannelID channelId, byte[] pageData)
        {
            if (!connected)
                AntConnected();

            if(pageData[0] == 0x10 && readingBasicInfo)
            {
                //Page 0x10 is power-only data. It is easy to parse, but power meters that send 0x11 or 0x12 do not send 0x10 often enough.
                //We will start out reading 0x10, but if we receive an 0x11 or 0x12, we will start ignoring 0x10.
                ParseBasicPowerData(pageData);
            }
            else if(pageData[0] == 0x11 || pageData[0] == 0x12)
            {
                //Pages 0x11 and 0x12 include more complicated torque data that will require us to do some calculations to get power numbers,
                //but if our power meter is sending these, they will not be sending the simpler 0x10 often enough to be useful.
                
                readingBasicInfo = false;
                ParseTorquePowerData(pageData);
            }
            else if (pageData[0] == 0x50)
            {
                //Page 0x50 contains the manufacturer ID, which we want for display purposes
                int manufacturerId = AntPowerMeterDevice.ParseManufacturerId(pageData);

                ZBConfig.UpdateAntPwrMtrManufacturerId(manufacturerId);
            }
            else if (pageData[0] == 0x52)
            {
                //Page 0x52 contains battery status information
                float batteryVoltageFractional = 100f * pageData[6] / 256;
                int batteryVoltageCoarse = pageData[7] & 0x0F;
                float batteryVoltageTotal = batteryVoltageCoarse + batteryVoltageFractional;

                int batteryStatusIndex = (pageData[7] & 0x70) >> 4;
                bool batteryLow = false;
                String batteryStatus = "";

                if (batteryStatusIndex == 0)
                    batteryStatus = "Reserved";
                else if (batteryStatusIndex == 1)
                    batteryStatus = "New";
                else if (batteryStatusIndex == 2)
                    batteryStatus = "Good";
                else if (batteryStatusIndex == 3)
                    batteryStatus = "OK";
                else if (batteryStatusIndex == 4)
                {
                    batteryStatus = "Low";
                    batteryLow = true;
                }
                else if (batteryStatusIndex == 5)
                {
                    batteryStatus = "Critical";
                    batteryLow = true;
                }
                else if (batteryStatusIndex == 6)
                    batteryStatus = "Reserved";
                else if (batteryStatusIndex == 7)
                    batteryStatus = "Invalid";

                if (ZBConfig.antDebugLogEnabled)
                    ZBState.Log("Battery voltage is: " + batteryVoltageTotal + "v -- Battery status is: " + batteryStatus);

                batteryStateCallback(batteryLow);
            }
        }

        protected override void ChannelClosed(byte channelNum)
        {
            AntDisconnected();
        }

        private void ParseBasicPowerData(byte[] pageData)
        {
            byte instPwrLsb = pageData[6];              //Least-significant bit of instantaneous power field
            byte instPwrMsb = pageData[7];              //Most-significant bit of instantaneous power field
            int instPwr = instPwrMsb << 8 | instPwrLsb; //Instantaneous power (in watts) as an int

            if(ZBConfig.antDebugLogEnabled)
                ZBState.Log("Got basic power data: " + instPwr + "w");

            if (powerUpdateCallback != null)
                powerUpdateCallback(instPwr);
        }

        private void ParseTorquePowerData(byte[] pageData)
        {
            int period = pageData[5] << 8 | pageData[4];
            int accumTorque = pageData[7] << 8 | pageData[6];

            if (period == previousPeriod && accumTorque == previousAccumTorque)
            {
                duplicatesReceived++;
                if(duplicatesReceived <= maxIgnoredDuplicates)
                    return;     //We get some duplicate sequential records that incorrectly calculate 0 watts. Ignore them unless enough come through that it is likely legitimate data (e.g. we stopped pedalling and we're at 0 watts)
            }
            else
                duplicatesReceived = 0;

            double accumTorqueDiff = accumTorque - previousAccumTorque;
            if (accumTorque < previousAccumTorque)
                accumTorqueDiff += 65536; //Rollover

            double periodDiff = period - previousPeriod;
            if (period < previousPeriod)
                periodDiff += 65536; //Rollover

            int power = 0;
            if(periodDiff > 0 && previousAccumTorque != 0 && previousPeriod != 0)   //Avoid divide by 0, and ignore the first reading we get because it's trash without something to compare it to
                power = (int)(pi128 * (accumTorqueDiff / periodDiff));  //Formula from ANT+ documentation D00001086_ANT+_Device_Profile_-_Bicycle_Power_Rev_5.1.pdf, Section 11 (Page 39) "Computing Power from Standard Torque Data Messages"

            previousPeriod = period;
            previousAccumTorque = accumTorque;

            if(ZBConfig.antDebugLogEnabled)
                ZBState.Log("Calculated torque power data: " + power + "w");

            if (powerUpdateCallback != null)
                powerUpdateCallback(power);
        }

        private void AntConnected()
        {
            connected = true;
            connectionStatusCallback(true);
        }

        private void AntDisconnected()
        {
            if(connected)
            {
                connected = false;
                connectionStatusCallback(false);
            }

            connectionResetTimer = new Timer(connectionResetDelay);
            connectionResetTimer.Elapsed += ConnectionResetTimerTick;
            connectionResetTimer.AutoReset = false;
            connectionResetTimer.Enabled = true;
        }

        private void ConnectionResetTimerTick(Object source, ElapsedEventArgs e)
        {
            connectionResetTimer.Enabled = false;
            connectionResetTimer.Dispose();
            connectionResetTimer = null;

            OpenChannel();
        }
    }
}
