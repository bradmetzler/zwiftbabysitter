﻿using System;
using System.Threading;

namespace ZwiftBabysitter
{
    internal class AntLedController
    {
        private byte currentLedZone = 0;
        private bool powerMeterBatteryIsLow = false;

        //Buffer of previous power readings
        private const int powerHistoryDepth = 80;           //At ~4Hz reception, this represents 20 seconds of messages
        private int powerHistoryIndex = -1;
        private long[] powerHistoryTimes = new long[powerHistoryDepth];
        private int[] powerHistoryWatts = new int[powerHistoryDepth];

        private Thread antThread;                           //This thread is used for starting and stopping the ANT+ communication
        private AntPowerMeterListener antListener;
        private readonly object antLock = new object();

        private LedMaestro ledMaestro;

        private AntDebugWindow antDebugWindow = null;
        private LedDebugWindow ledDebugWindow = null;

        //Power zone boundaries (in watts)
        private int zone1Min = 0;
        private int zone2Min = 0;
        private int zone3Min = 0;
        private int zone4Min = 0;
        private int zone5Min = 0;
        private int zone6Min = 0;

        public AntLedController()
        {
            antListener = new AntPowerMeterListener(NewPowerDataReceived, BatteryStateReceived, AntConnectionStatusChanged, AntConnectionError);
            ledMaestro = new LedMaestro(LedLightConnected);

            //These differ slightly from the Coggan 7-zone model, but match the Zwift 6-zone model and are very close to the Strava model
            zone1Min = 1;                                           //Active Recovery
            zone2Min = (int)Math.Floor(ZBConfig.ftpWatts * 0.6);    //Endurance
            zone3Min = (int)Math.Floor(ZBConfig.ftpWatts * 0.76);   //Tempo
            zone4Min = (int)Math.Floor(ZBConfig.ftpWatts * 0.9);    //Threshold
            zone5Min = (int)Math.Floor(ZBConfig.ftpWatts * 1.05);   //VO2Max
            zone6Min = (int)Math.Floor(ZBConfig.ftpWatts * 1.18);   //Anaerobic
        }

        public void Start()
        {
            if (ZBConfig.antDebugWindowEnabled)
            {
                antDebugWindow = new AntDebugWindow();
                antDebugWindow.Show();
            }

            if (ZBConfig.ledDebugWindowEnabled)
            {
                ledDebugWindow = new LedDebugWindow(SendLedUpdate);
                ledDebugWindow.Show();
            }

            //ANT+ initialization can take a few seconds. Start the listener in its own thread so that the UI doesn't hang
            antThread = new Thread(new ThreadStart(AntStartThreadProc));
            antThread.Start();

            ledMaestro.FindLeds();
        }

        public void Stop(bool appClosing)
        {
            if (antListener != null)
            {
                //ANT+ cleanup can take a few seconds. Stop the listener in its own thread so that the UI doesn't hang
                antThread = new Thread(new ThreadStart(AntStopThreadProc));
                antThread.Start();
            }

            if (ledMaestro != null)
                ledMaestro.ResetLedsAndDisconnect(appClosing);

            if (antDebugWindow != null)
            {
                antDebugWindow.CloseSafely();
                antDebugWindow = null;
            }

            if (ledDebugWindow != null)
            {
                ledDebugWindow.CloseSafely();
                ledDebugWindow = null;
            }
        }

        public void AntStartThreadProc()
        {
            lock (antLock)
            {
                antListener.StartListening();
            }
        }
        public void AntStopThreadProc()
        {
            lock (antLock)
            {
                antListener.StopListening();
            }
        }

        public void AntConnectionError()
        {
            Stop(false);
        }

        public void AntConnectionStatusChanged(bool connected)
        {
            if(connected)
            {
                ZBState.Log("ANT+ connected to " + ZBConfig.powerMeter.GetPrettyFormat());

                if (antDebugWindow != null)
                    antDebugWindow.UpdatePowerData(0, 0);
            }
            else
            {
                ZBState.Log("ANT+ lost connection to " + ZBConfig.powerMeter.GetPrettyFormat());

                if (antDebugWindow != null)
                    antDebugWindow.UpdatePowerData(-1, 0);
            }

            if (ledMaestro != null)
                ledMaestro.IndicatePowerMeterStatusChange(connected);
        }

        public void BatteryStateReceived(bool batteryLow)
        {
            if (powerMeterBatteryIsLow == batteryLow)
                return;

            powerMeterBatteryIsLow = batteryLow;
            if(ledMaestro != null && batteryLow)
                ledMaestro.IndicateLowPowerMeterBattery();
            if (antDebugWindow != null)
                antDebugWindow.UpdateBatteryLow(batteryLow);
        }

        public void NewPowerDataReceived(int newPower)
        {
            //Add the new power reading to the record
            powerHistoryIndex++;
            if (powerHistoryIndex >= powerHistoryDepth)
                powerHistoryIndex = 0;
            powerHistoryTimes[powerHistoryIndex] = ZBState.CurrentTimeMillis();
            powerHistoryWatts[powerHistoryIndex] = newPower;

            //Determine appropriate LED status
            if(UpdateLedStatus())
                SendLedUpdate(currentLedZone);

            //Update the ANT+ Debug window if it is enabled
            if(antDebugWindow != null)
                antDebugWindow.UpdatePowerData(newPower, GetZone(newPower));
        }

        //Determines the appropriate LED state, based on a few simple rules to smooth out minor zone fluctuations but preserve larger ones.
        //Returns true if the LED state has changed; false otherwise
        private bool UpdateLedStatus()
        {
            int newPower = powerHistoryWatts[powerHistoryIndex];
            long newTime = powerHistoryTimes[powerHistoryIndex];
            byte newZone = GetZone(newPower);   //Zone representing the power update we just received

            //RULE 1: If the new zone and the old zone are the same, do nothing
            if (newZone == currentLedZone)
                return false;

            //RULE 2: Jumps of greater than 1 zone should be instantly reflected (zone 3 straight into a big sprint, for instance)
            if (Math.Abs(newZone - currentLedZone) > 1 || currentLedZone == 0)
            {
                currentLedZone = newZone;
                return true;
            }

            //RULE 3: Jumps of greater than 50 watts should be instantly reflected, even if they are in adjacent zones
            int previousHistoryIndex = powerHistoryIndex - 1;
            if (previousHistoryIndex < 0)
                previousHistoryIndex = powerHistoryDepth - 1;
            long previousPowerTime = powerHistoryTimes[previousHistoryIndex];
            int previousPowerWatts = powerHistoryWatts[previousHistoryIndex];
            
            if (Math.Abs(newPower - previousPowerWatts) > 50)
            {
                currentLedZone = newZone;
                return true;
            }

            //RULE 4: This means we have crossed into an adjacent zone by less than 50 watts. This can commonly happen in the middle of an
            //interval whose target wattage is near a zone boundary. We don't want to constantly flash the LEDs between 2 colors/patterns
            //in this instance, so we should try to smooth this out. In this situation, only reflect the new zone if we've held it for 5 seconds.
            for(int i = 0; i < powerHistoryDepth; i++)  //Bail out if we read the entire history. This prevents an infinite loop in the weird scenario where the entire buffer is filled with less than 5 seconds of data.
            {
                //If the buffer is not full of data and we run out of data, return.
                if (previousPowerTime == 0)
                {
                    return false;
                }

                //If we reach to 5 seconds and it's all been the same zone as the new zone, update it and return.
                if (newTime - previousPowerTime >= 5000)
                {
                    currentLedZone = newZone;
                    return true;
                }

                //If we reach a zone that is not the new zone, return. This indicates rapid zone changes indicative of riding near a zone boundary.
                if (GetZone(previousPowerWatts) != newZone)
                {
                    return false;
                }

                //Go back in history another step
                previousHistoryIndex--;
                if (previousHistoryIndex < 0)
                    previousHistoryIndex = powerHistoryDepth - 1;
                previousPowerTime = powerHistoryTimes[previousHistoryIndex];
                previousPowerWatts = powerHistoryWatts[previousHistoryIndex];
            }

            ZBState.Log("...should never see this...");
            return false;
        }

        public void LedLightConnected(int connectedLeds)
        {
            if(ledDebugWindow != null)
                ledDebugWindow.UpdateConnectedLeds(connectedLeds);
        }

        public void SendLedUpdate(byte zone)
        {
            if(!powerMeterBatteryIsLow)
                ledMaestro.ChangeToZone(zone);          //If power meter battery is low, skip updating the LEDs based on zone, so that the user will continue to see indication that the battery is low.

            if (ledDebugWindow != null)
                ledDebugWindow.UpdateLedZone(zone);
        }

        private byte GetZone(int watts)
        {
            if (watts >= zone6Min)
                return 6;
            if (watts >= zone5Min)
                return 5;
            if (watts >= zone4Min)
                return 4;
            if (watts >= zone3Min)
                return 3;
            if (watts >= zone2Min)
                return 2;
            if (watts >= zone1Min)
                return 1;

            return 0;
        }
    }
}
