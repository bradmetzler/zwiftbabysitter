﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZwiftBabysitter
{
    internal class ZBConfig
    {
        public static readonly int maxAntSearchChannels = 8;    //This appears to be the maximum supported concurrent channels

        private static readonly String configFileName = "ZwiftBabysitter.cfg";
        public static bool configIsValid = false;

        public static int mainTimerInterval;
        public static bool virtualHereEnabled;
        public static String virtualHereExe;
        public static String virtualHereProcName;
        public static String virtualHereArgs;
        public static bool borderlessGamingEnabled;
        public static String borderlessGamingExe;
        public static String borderlessGamingProcName;
        public static String borderlessGamingArgs;
        public static String zwiftLauncherExe;
        public static String zwiftLauncherProcName;
        public static String zwiftLauncherArgs;
        public static String zwiftExe;
        public static String zwiftProcName;

        public static bool ledsEnabled;
        public static bool antDebugLogEnabled;
        public static bool antDebugWindowEnabled;
        public static int antUsbId = -1;
        public static AntPowerMeterDevice powerMeter;
        public static int ftpWatts;
        public static bool ledDebugLogEnabled;
        public static bool ledDebugWindowEnabled;
        public static List<LedLight> ledBulbs;
        public static List<LedLight> ledStrips;

        public static void LoadConfig()
        {
            powerMeter = new AntPowerMeterDevice();
            ledBulbs = new List<LedLight>();
            ledStrips = new List<LedLight>();

            String[] configLines = System.IO.File.ReadAllLines(configFileName);

            if (configLines.Length != 59)
            {
                throw new Exception("Config file was malformed.");
            }

            if (!int.TryParse(configLines[1], out mainTimerInterval))
                mainTimerInterval = 30000;

            if (!bool.TryParse(configLines[4], out virtualHereEnabled))
                virtualHereEnabled = false;

            if (!bool.TryParse(configLines[13], out borderlessGamingEnabled))
                borderlessGamingEnabled = false;

            virtualHereExe = configLines[7];
            if(virtualHereEnabled)
                virtualHereProcName = GetProcNameFromExe(virtualHereExe);
            virtualHereArgs = configLines[10];
            borderlessGamingExe = configLines[16];
            if(borderlessGamingEnabled)
                borderlessGamingProcName = GetProcNameFromExe(borderlessGamingExe);
            borderlessGamingArgs = configLines[19];
            zwiftLauncherExe = configLines[22];
            zwiftLauncherProcName = GetProcNameFromExe(zwiftLauncherExe);
            zwiftLauncherArgs = configLines[25];
            zwiftExe = configLines[28];
            zwiftProcName = GetProcNameFromExe(zwiftExe);

            if (!bool.TryParse(configLines[31], out ledsEnabled))
                ledsEnabled = false;

            if (!bool.TryParse(configLines[34], out antDebugLogEnabled))
                antDebugLogEnabled = false;

            if (!bool.TryParse(configLines[37], out antDebugWindowEnabled))
                antDebugWindowEnabled = false;

            if (!int.TryParse(configLines[40], out antUsbId))
                antUsbId = -1;

            String[] powerMeterRaw = configLines[43].Split(' ');
            if(powerMeterRaw.Length == 3)
            {
                int.TryParse(powerMeterRaw[0], out powerMeter.deviceNumber);
                int.TryParse(powerMeterRaw[1], out powerMeter.transmissionTypeId);
                int.TryParse(powerMeterRaw[2], out powerMeter.manufacturerId);
            }

            if (!int.TryParse(configLines[46], out ftpWatts))
                ftpWatts = 0;

            if (!bool.TryParse(configLines[49], out ledDebugLogEnabled))
                ledDebugLogEnabled = false;

            if (!bool.TryParse(configLines[52], out ledDebugWindowEnabled))
                ledDebugWindowEnabled = false;

            DeserializeLedLights(configLines[55], ledBulbs);
            DeserializeLedLights(configLines[58], ledStrips);

            //Validate settings
            if (virtualHereEnabled && !System.IO.File.Exists(virtualHereExe))
                throw new Exception("VirtualHere executable not found.");
            if (borderlessGamingEnabled && !System.IO.File.Exists(borderlessGamingExe))
                throw new Exception("BorderlessGaming executable not found.");
            if (!System.IO.File.Exists(zwiftLauncherExe))
                throw new Exception("ZwiftLauncer executable not found.");
            if (!System.IO.File.Exists(zwiftExe))
                throw new Exception("Zwift executable not found.");

            configIsValid = true;
        }

        public static void WriteConfig()
        {
            String[] configLines = new String[59];
            configLines[0] = "[Timer Interval (milliseconds)]";
            configLines[1] = Convert.ToString(mainTimerInterval);
            configLines[2] = String.Empty;
            configLines[3] = "[VirtualHere Enabled]";
            configLines[4] = Convert.ToString(virtualHereEnabled);
            configLines[5] = String.Empty;
            configLines[6] = "[VirtualHere Executable]";
            configLines[7] = virtualHereExe;
            configLines[8] = String.Empty;
            configLines[9] = "[VirtualHere Arguments]";
            configLines[10] = virtualHereArgs;
            configLines[11] = String.Empty;
            configLines[12] = "[BorderlessGaming Enabled]";
            configLines[13] = Convert.ToString(borderlessGamingEnabled);
            configLines[14] = String.Empty;
            configLines[15] = "[BorderlessGaming Executable]";
            configLines[16] = borderlessGamingExe;
            configLines[17] = String.Empty;
            configLines[18] = "[BorderlessGaming Arguments]";
            configLines[19] = borderlessGamingArgs;
            configLines[20] = String.Empty;
            configLines[21] = "[ZwiftLauncher Executable]";
            configLines[22] = zwiftLauncherExe;
            configLines[23] = String.Empty;
            configLines[24] = "[ZwiftLauncher Arguments]";
            configLines[25] = zwiftLauncherArgs;
            configLines[26] = String.Empty;
            configLines[27] = "[Zwift Executable]";
            configLines[28] = zwiftExe;
            configLines[29] = String.Empty;
            configLines[30] = "[LEDs Enabled]";
            configLines[31] = Convert.ToString(ledsEnabled);
            configLines[32] = String.Empty;
            configLines[33] = "[ANT+ Debug Log Enabled]";
            configLines[34] = Convert.ToString(antDebugLogEnabled);
            configLines[35] = String.Empty;
            configLines[36] = "[ANT+ Debug Window Enabled]";
            configLines[37] = Convert.ToString(antDebugWindowEnabled);
            configLines[38] = String.Empty;
            configLines[39] = "[ANT+ USB ID]";
            configLines[40] = Convert.ToString(antUsbId);
            configLines[41] = String.Empty;
            configLines[42] = "[ANT+ Power Meter]";
            configLines[43] = Convert.ToString(powerMeter.deviceNumber) + " " + Convert.ToString(powerMeter.transmissionTypeId) + " " + Convert.ToString(powerMeter.manufacturerId);
            configLines[44] = String.Empty;
            configLines[45] = "[FTP Watts]";
            configLines[46] = Convert.ToString(ftpWatts);
            configLines[47] = String.Empty;
            configLines[48] = "[LED Debug Log Enabled]";
            configLines[49] = Convert.ToString(ledDebugLogEnabled);
            configLines[50] = String.Empty;
            configLines[51] = "[LED Debug Window Enabled]";
            configLines[52] = Convert.ToString(ledDebugWindowEnabled);
            configLines[53] = String.Empty;
            configLines[54] = "[LED Bulbs]";
            configLines[55] = SerializeLedLights(ledBulbs);
            configLines[56] = String.Empty;
            configLines[57] = "[LED Strips]";
            configLines[58] = SerializeLedLights(ledStrips);

            System.IO.File.WriteAllLines(configFileName, configLines);
            configIsValid = true;
        }

        //This will be called periodically by the ANT+ Power Meter Listener.
        //In case the manufacturer information was never discovered during the initial scan,
        //we will use this opportunity to make sure we have it saved for future display.
        public static void UpdateAntPwrMtrManufacturerId(int manufacturerId)
        {
            if (manufacturerId == powerMeter.manufacturerId)
                return;

            ZBState.Log("Updating stored ANT+ Power Meter Manufacturer ID");
            powerMeter.manufacturerId = manufacturerId;
            WriteConfig();
        }

        private static void DeserializeLedLights(String raw, List<LedLight> parsed)
        {
            try
            {
                String[] rawParts = raw.Split('|');

                int numLights = 0;
                int.TryParse(rawParts[0], out numLights);

                for(int i = 1; i <= numLights; i++)
                    parsed.Add(new LedLight(rawParts[(i * 2) - 1], rawParts[i * 2]));
            }
            catch(Exception e)
            {
                parsed.Clear();
                ZBState.Log("Failed to read LED Bulbs from config. " + e.ToString());
            }
        }

        private static String SerializeLedLights(List<LedLight> ledLights)
        {
            StringBuilder result = new StringBuilder();

            result.Append(ledLights.Count);

            foreach(LedLight ledLight in ledLights)
            {
                result.Append('|');
                result.Append(ledLight.deviceId);
                result.Append('|');
                result.Append(ledLight.deviceName);
            }

            return result.ToString();
        }

        private static String GetProcNameFromExe(String exe)
        {
            int slashLoc = exe.LastIndexOf("\\");
            int exeLoc = exe.LastIndexOf(".exe");
            return exe.Substring(slashLoc + 1, exeLoc - slashLoc - 1);
        }
    }
}
