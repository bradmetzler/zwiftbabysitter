﻿namespace ZwiftBabysitter
{
    partial class SettingsWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsWindow));
            this.label1 = new System.Windows.Forms.Label();
            this.numTimerInterval = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbVirtualHereArgs = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnVirtualHereBrowse = new System.Windows.Forms.Button();
            this.tbVirtualHereLocation = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbVirtualHereEnabled = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbBorderlessGamingArgs = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnBorderlessGamingBrowse = new System.Windows.Forms.Button();
            this.tbBorderlessGamingLocation = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cbBorderlessGamingEnabled = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tbZwiftLauncherArgs = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnZwiftLauncherBrowse = new System.Windows.Forms.Button();
            this.tbZwiftLauncherLocation = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnZwiftAppBrowse = new System.Windows.Forms.Button();
            this.tbZwiftAppLocation = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.cbLedDebugLogEnabled = new System.Windows.Forms.CheckBox();
            this.cbLedDebugWindowEnabled = new System.Windows.Forms.CheckBox();
            this.btnLedChoose = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.tbLedStripDisplay = new System.Windows.Forms.TextBox();
            this.tbLedBulbDisplay = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lblUsbIdInfo = new System.Windows.Forms.LinkLabel();
            this.numUsbId = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.cbAntDebugLogEnabled = new System.Windows.Forms.CheckBox();
            this.cbAntDebugWindowEnabled = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tbPowerMeterDisplay = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.numFtpWatts = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.btnPowerMeterChoose = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.cbLedsEnabled = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.numTimerInterval)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUsbId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numFtpWatts)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(156, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Timer Interval (milliseconds)";
            // 
            // numTimerInterval
            // 
            this.numTimerInterval.Location = new System.Drawing.Point(10, 28);
            this.numTimerInterval.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.numTimerInterval.Maximum = new decimal(new int[] {
            600000,
            0,
            0,
            0});
            this.numTimerInterval.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numTimerInterval.Name = "numTimerInterval";
            this.numTimerInterval.Size = new System.Drawing.Size(140, 23);
            this.numTimerInterval.TabIndex = 1;
            this.numTimerInterval.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbVirtualHereArgs);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btnVirtualHereBrowse);
            this.groupBox1.Controls.Add(this.tbVirtualHereLocation);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cbVirtualHereEnabled);
            this.groupBox1.Location = new System.Drawing.Point(10, 58);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox1.Size = new System.Drawing.Size(416, 112);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "VirtualHere";
            // 
            // tbVirtualHereArgs
            // 
            this.tbVirtualHereArgs.Location = new System.Drawing.Point(74, 72);
            this.tbVirtualHereArgs.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tbVirtualHereArgs.Name = "tbVirtualHereArgs";
            this.tbVirtualHereArgs.Size = new System.Drawing.Size(236, 23);
            this.tbVirtualHereArgs.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 75);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "Args:";
            // 
            // btnVirtualHereBrowse
            // 
            this.btnVirtualHereBrowse.Location = new System.Drawing.Point(317, 40);
            this.btnVirtualHereBrowse.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnVirtualHereBrowse.Name = "btnVirtualHereBrowse";
            this.btnVirtualHereBrowse.Size = new System.Drawing.Size(88, 27);
            this.btnVirtualHereBrowse.TabIndex = 3;
            this.btnVirtualHereBrowse.Text = "Browse...";
            this.btnVirtualHereBrowse.UseVisualStyleBackColor = true;
            this.btnVirtualHereBrowse.Click += new System.EventHandler(this.BtnVirtualHereBrowse_Click);
            // 
            // tbVirtualHereLocation
            // 
            this.tbVirtualHereLocation.Location = new System.Drawing.Point(74, 43);
            this.tbVirtualHereLocation.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tbVirtualHereLocation.Name = "tbVirtualHereLocation";
            this.tbVirtualHereLocation.Size = new System.Drawing.Size(236, 23);
            this.tbVirtualHereLocation.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 46);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Location:";
            // 
            // cbVirtualHereEnabled
            // 
            this.cbVirtualHereEnabled.AutoSize = true;
            this.cbVirtualHereEnabled.Location = new System.Drawing.Point(10, 22);
            this.cbVirtualHereEnabled.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbVirtualHereEnabled.Name = "cbVirtualHereEnabled";
            this.cbVirtualHereEnabled.Size = new System.Drawing.Size(68, 19);
            this.cbVirtualHereEnabled.TabIndex = 0;
            this.cbVirtualHereEnabled.Text = "Enabled";
            this.cbVirtualHereEnabled.UseVisualStyleBackColor = true;
            this.cbVirtualHereEnabled.CheckedChanged += new System.EventHandler(this.CbVirtualHereEnabled_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tbBorderlessGamingArgs);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.btnBorderlessGamingBrowse);
            this.groupBox2.Controls.Add(this.tbBorderlessGamingLocation);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.cbBorderlessGamingEnabled);
            this.groupBox2.Location = new System.Drawing.Point(10, 177);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox2.Size = new System.Drawing.Size(416, 112);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "BorderlessGaming";
            // 
            // tbBorderlessGamingArgs
            // 
            this.tbBorderlessGamingArgs.Location = new System.Drawing.Point(74, 72);
            this.tbBorderlessGamingArgs.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tbBorderlessGamingArgs.Name = "tbBorderlessGamingArgs";
            this.tbBorderlessGamingArgs.Size = new System.Drawing.Size(236, 23);
            this.tbBorderlessGamingArgs.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(30, 75);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 15);
            this.label4.TabIndex = 4;
            this.label4.Text = "Args:";
            // 
            // btnBorderlessGamingBrowse
            // 
            this.btnBorderlessGamingBrowse.Location = new System.Drawing.Point(317, 40);
            this.btnBorderlessGamingBrowse.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnBorderlessGamingBrowse.Name = "btnBorderlessGamingBrowse";
            this.btnBorderlessGamingBrowse.Size = new System.Drawing.Size(88, 27);
            this.btnBorderlessGamingBrowse.TabIndex = 3;
            this.btnBorderlessGamingBrowse.Text = "Browse...";
            this.btnBorderlessGamingBrowse.UseVisualStyleBackColor = true;
            this.btnBorderlessGamingBrowse.Click += new System.EventHandler(this.BtnBorderlessGamingBrowse_Click);
            // 
            // tbBorderlessGamingLocation
            // 
            this.tbBorderlessGamingLocation.Location = new System.Drawing.Point(74, 43);
            this.tbBorderlessGamingLocation.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tbBorderlessGamingLocation.Name = "tbBorderlessGamingLocation";
            this.tbBorderlessGamingLocation.Size = new System.Drawing.Size(236, 23);
            this.tbBorderlessGamingLocation.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 46);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 15);
            this.label5.TabIndex = 1;
            this.label5.Text = "Location:";
            // 
            // cbBorderlessGamingEnabled
            // 
            this.cbBorderlessGamingEnabled.AutoSize = true;
            this.cbBorderlessGamingEnabled.Location = new System.Drawing.Point(10, 22);
            this.cbBorderlessGamingEnabled.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbBorderlessGamingEnabled.Name = "cbBorderlessGamingEnabled";
            this.cbBorderlessGamingEnabled.Size = new System.Drawing.Size(68, 19);
            this.cbBorderlessGamingEnabled.TabIndex = 0;
            this.cbBorderlessGamingEnabled.Text = "Enabled";
            this.cbBorderlessGamingEnabled.UseVisualStyleBackColor = true;
            this.cbBorderlessGamingEnabled.CheckedChanged += new System.EventHandler(this.CbBorderlessGamingEnabled_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tbZwiftLauncherArgs);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.btnZwiftLauncherBrowse);
            this.groupBox3.Controls.Add(this.tbZwiftLauncherLocation);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Location = new System.Drawing.Point(10, 295);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox3.Size = new System.Drawing.Size(416, 83);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "ZwiftLauncher";
            // 
            // tbZwiftLauncherArgs
            // 
            this.tbZwiftLauncherArgs.Location = new System.Drawing.Point(74, 48);
            this.tbZwiftLauncherArgs.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tbZwiftLauncherArgs.Name = "tbZwiftLauncherArgs";
            this.tbZwiftLauncherArgs.Size = new System.Drawing.Size(236, 23);
            this.tbZwiftLauncherArgs.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(30, 52);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 15);
            this.label6.TabIndex = 4;
            this.label6.Text = "Args:";
            // 
            // btnZwiftLauncherBrowse
            // 
            this.btnZwiftLauncherBrowse.Location = new System.Drawing.Point(317, 17);
            this.btnZwiftLauncherBrowse.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnZwiftLauncherBrowse.Name = "btnZwiftLauncherBrowse";
            this.btnZwiftLauncherBrowse.Size = new System.Drawing.Size(88, 27);
            this.btnZwiftLauncherBrowse.TabIndex = 3;
            this.btnZwiftLauncherBrowse.Text = "Browse...";
            this.btnZwiftLauncherBrowse.UseVisualStyleBackColor = true;
            this.btnZwiftLauncherBrowse.Click += new System.EventHandler(this.BtnZwiftLauncherBrowse_Click);
            // 
            // tbZwiftLauncherLocation
            // 
            this.tbZwiftLauncherLocation.Location = new System.Drawing.Point(74, 20);
            this.tbZwiftLauncherLocation.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tbZwiftLauncherLocation.Name = "tbZwiftLauncherLocation";
            this.tbZwiftLauncherLocation.Size = new System.Drawing.Size(236, 23);
            this.tbZwiftLauncherLocation.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 23);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 15);
            this.label7.TabIndex = 1;
            this.label7.Text = "Location:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnZwiftAppBrowse);
            this.groupBox4.Controls.Add(this.tbZwiftAppLocation);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Location = new System.Drawing.Point(10, 385);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox4.Size = new System.Drawing.Size(416, 53);
            this.groupBox4.TabIndex = 8;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "ZwiftApp";
            // 
            // btnZwiftAppBrowse
            // 
            this.btnZwiftAppBrowse.Location = new System.Drawing.Point(317, 17);
            this.btnZwiftAppBrowse.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnZwiftAppBrowse.Name = "btnZwiftAppBrowse";
            this.btnZwiftAppBrowse.Size = new System.Drawing.Size(88, 27);
            this.btnZwiftAppBrowse.TabIndex = 3;
            this.btnZwiftAppBrowse.Text = "Browse...";
            this.btnZwiftAppBrowse.UseVisualStyleBackColor = true;
            this.btnZwiftAppBrowse.Click += new System.EventHandler(this.BtnZwiftAppBrowse_Click);
            // 
            // tbZwiftAppLocation
            // 
            this.tbZwiftAppLocation.Location = new System.Drawing.Point(74, 20);
            this.tbZwiftAppLocation.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tbZwiftAppLocation.Name = "tbZwiftAppLocation";
            this.tbZwiftAppLocation.Size = new System.Drawing.Size(236, 23);
            this.tbZwiftAppLocation.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 23);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 15);
            this.label9.TabIndex = 1;
            this.label9.Text = "Location:";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(268, 494);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(88, 27);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(363, 494);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(88, 27);
            this.btnCancel.TabIndex = 10;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(8, 8);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(447, 479);
            this.tabControl1.TabIndex = 11;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.numTimerInterval);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tabPage1.Size = new System.Drawing.Size(439, 451);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "General Settings";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox6);
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.cbLedsEnabled);
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tabPage2.Size = new System.Drawing.Size(439, 451);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "ANT+/LED Settings";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.cbLedDebugLogEnabled);
            this.groupBox6.Controls.Add(this.cbLedDebugWindowEnabled);
            this.groupBox6.Controls.Add(this.btnLedChoose);
            this.groupBox6.Controls.Add(this.label14);
            this.groupBox6.Controls.Add(this.tbLedStripDisplay);
            this.groupBox6.Controls.Add(this.tbLedBulbDisplay);
            this.groupBox6.Controls.Add(this.label13);
            this.groupBox6.Location = new System.Drawing.Point(10, 243);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox6.Size = new System.Drawing.Size(416, 140);
            this.groupBox6.TabIndex = 11;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Lights";
            // 
            // cbLedDebugLogEnabled
            // 
            this.cbLedDebugLogEnabled.AutoSize = true;
            this.cbLedDebugLogEnabled.Location = new System.Drawing.Point(7, 22);
            this.cbLedDebugLogEnabled.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbLedDebugLogEnabled.Name = "cbLedDebugLogEnabled";
            this.cbLedDebugLogEnabled.Size = new System.Drawing.Size(144, 19);
            this.cbLedDebugLogEnabled.TabIndex = 13;
            this.cbLedDebugLogEnabled.Text = "Show LED Debug Logs";
            this.cbLedDebugLogEnabled.UseVisualStyleBackColor = true;
            // 
            // cbLedDebugWindowEnabled
            // 
            this.cbLedDebugWindowEnabled.AutoSize = true;
            this.cbLedDebugWindowEnabled.Location = new System.Drawing.Point(7, 48);
            this.cbLedDebugWindowEnabled.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbLedDebugWindowEnabled.Name = "cbLedDebugWindowEnabled";
            this.cbLedDebugWindowEnabled.Size = new System.Drawing.Size(163, 19);
            this.cbLedDebugWindowEnabled.TabIndex = 14;
            this.cbLedDebugWindowEnabled.Text = "Show LED Debug Window";
            this.cbLedDebugWindowEnabled.UseVisualStyleBackColor = true;
            // 
            // btnLedChoose
            // 
            this.btnLedChoose.Location = new System.Drawing.Point(322, 75);
            this.btnLedChoose.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnLedChoose.Name = "btnLedChoose";
            this.btnLedChoose.Size = new System.Drawing.Size(88, 53);
            this.btnLedChoose.TabIndex = 10;
            this.btnLedChoose.Text = "Choose...";
            this.btnLedChoose.UseVisualStyleBackColor = true;
            this.btnLedChoose.Click += new System.EventHandler(this.BtnLedChoose_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(12, 108);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(70, 15);
            this.label14.TabIndex = 12;
            this.label14.Text = "LED Strip(s):";
            // 
            // tbLedStripDisplay
            // 
            this.tbLedStripDisplay.Enabled = false;
            this.tbLedStripDisplay.Location = new System.Drawing.Point(96, 105);
            this.tbLedStripDisplay.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tbLedStripDisplay.Name = "tbLedStripDisplay";
            this.tbLedStripDisplay.Size = new System.Drawing.Size(219, 23);
            this.tbLedStripDisplay.TabIndex = 11;
            // 
            // tbLedBulbDisplay
            // 
            this.tbLedBulbDisplay.Enabled = false;
            this.tbLedBulbDisplay.Location = new System.Drawing.Point(96, 75);
            this.tbLedBulbDisplay.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tbLedBulbDisplay.Name = "tbLedBulbDisplay";
            this.tbLedBulbDisplay.Size = new System.Drawing.Size(219, 23);
            this.tbLedBulbDisplay.TabIndex = 10;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(12, 78);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(70, 15);
            this.label13.TabIndex = 10;
            this.label13.Text = "LED Bulb(s):";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.lblUsbIdInfo);
            this.groupBox5.Controls.Add(this.numUsbId);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.cbAntDebugLogEnabled);
            this.groupBox5.Controls.Add(this.cbAntDebugWindowEnabled);
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Controls.Add(this.tbPowerMeterDisplay);
            this.groupBox5.Controls.Add(this.label11);
            this.groupBox5.Controls.Add(this.numFtpWatts);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this.btnPowerMeterChoose);
            this.groupBox5.Location = new System.Drawing.Point(10, 69);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox5.Size = new System.Drawing.Size(416, 167);
            this.groupBox5.TabIndex = 10;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "ANT+";
            // 
            // lblUsbIdInfo
            // 
            this.lblUsbIdInfo.AutoSize = true;
            this.lblUsbIdInfo.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.lblUsbIdInfo.Location = new System.Drawing.Point(164, 77);
            this.lblUsbIdInfo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUsbIdInfo.Name = "lblUsbIdInfo";
            this.lblUsbIdInfo.Size = new System.Drawing.Size(72, 15);
            this.lblUsbIdInfo.TabIndex = 12;
            this.lblUsbIdInfo.TabStop = true;
            this.lblUsbIdInfo.Text = "What\'s This?";
            this.lblUsbIdInfo.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LblUsbIdInfo_LinkClicked);
            // 
            // numUsbId
            // 
            this.numUsbId.Location = new System.Drawing.Point(96, 75);
            this.numUsbId.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.numUsbId.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numUsbId.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.numUsbId.Name = "numUsbId";
            this.numUsbId.Size = new System.Drawing.Size(62, 23);
            this.numUsbId.TabIndex = 11;
            this.numUsbId.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numUsbId.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(35, 77);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(45, 15);
            this.label15.TabIndex = 10;
            this.label15.Text = "USB ID:";
            // 
            // cbAntDebugLogEnabled
            // 
            this.cbAntDebugLogEnabled.AutoSize = true;
            this.cbAntDebugLogEnabled.Location = new System.Drawing.Point(7, 22);
            this.cbAntDebugLogEnabled.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbAntDebugLogEnabled.Name = "cbAntDebugLogEnabled";
            this.cbAntDebugLogEnabled.Size = new System.Drawing.Size(155, 19);
            this.cbAntDebugLogEnabled.TabIndex = 1;
            this.cbAntDebugLogEnabled.Text = "Show ANT+ Debug Logs";
            this.cbAntDebugLogEnabled.UseVisualStyleBackColor = true;
            // 
            // cbAntDebugWindowEnabled
            // 
            this.cbAntDebugWindowEnabled.AutoSize = true;
            this.cbAntDebugWindowEnabled.Location = new System.Drawing.Point(7, 48);
            this.cbAntDebugWindowEnabled.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbAntDebugWindowEnabled.Name = "cbAntDebugWindowEnabled";
            this.cbAntDebugWindowEnabled.Size = new System.Drawing.Size(174, 19);
            this.cbAntDebugWindowEnabled.TabIndex = 6;
            this.cbAntDebugWindowEnabled.Text = "Show ANT+ Debug Window";
            this.cbAntDebugWindowEnabled.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(159, 137);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(35, 15);
            this.label12.TabIndex = 9;
            this.label12.Text = "watts";
            // 
            // tbPowerMeterDisplay
            // 
            this.tbPowerMeterDisplay.Enabled = false;
            this.tbPowerMeterDisplay.Location = new System.Drawing.Point(96, 105);
            this.tbPowerMeterDisplay.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tbPowerMeterDisplay.Name = "tbPowerMeterDisplay";
            this.tbPowerMeterDisplay.Size = new System.Drawing.Size(219, 23);
            this.tbPowerMeterDisplay.TabIndex = 4;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(54, 137);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(29, 15);
            this.label11.TabIndex = 7;
            this.label11.Text = "FTP:";
            // 
            // numFtpWatts
            // 
            this.numFtpWatts.Location = new System.Drawing.Point(96, 135);
            this.numFtpWatts.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.numFtpWatts.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numFtpWatts.Name = "numFtpWatts";
            this.numFtpWatts.Size = new System.Drawing.Size(62, 23);
            this.numFtpWatts.TabIndex = 8;
            this.numFtpWatts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 108);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 15);
            this.label10.TabIndex = 3;
            this.label10.Text = "Power Meter:";
            // 
            // btnPowerMeterChoose
            // 
            this.btnPowerMeterChoose.Location = new System.Drawing.Point(322, 103);
            this.btnPowerMeterChoose.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnPowerMeterChoose.Name = "btnPowerMeterChoose";
            this.btnPowerMeterChoose.Size = new System.Drawing.Size(88, 27);
            this.btnPowerMeterChoose.TabIndex = 5;
            this.btnPowerMeterChoose.Text = "Choose...";
            this.btnPowerMeterChoose.UseVisualStyleBackColor = true;
            this.btnPowerMeterChoose.Click += new System.EventHandler(this.BtnPowerMeterChoose_Click);
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.label8.Location = new System.Drawing.Point(27, 24);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(327, 32);
            this.label8.TabIndex = 2;
            this.label8.Text = "(requires ANT+ power meter, dedicated ANT+ transceiver, and Yeelight LED bulbs an" +
    "d/or strips on the same LAN)";
            // 
            // cbLedsEnabled
            // 
            this.cbLedsEnabled.AutoSize = true;
            this.cbLedsEnabled.Location = new System.Drawing.Point(7, 7);
            this.cbLedsEnabled.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbLedsEnabled.Name = "cbLedsEnabled";
            this.cbLedsEnabled.Size = new System.Drawing.Size(211, 19);
            this.cbLedsEnabled.TabIndex = 0;
            this.cbLedsEnabled.Text = "Control LEDs Based on Power Zone";
            this.cbLedsEnabled.UseVisualStyleBackColor = true;
            this.cbLedsEnabled.CheckedChanged += new System.EventHandler(this.CbLedsEnabled_CheckedChanged);
            // 
            // SettingsWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(462, 531);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsWindow";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Settings";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.numTimerInterval)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUsbId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numFtpWatts)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numTimerInterval;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbVirtualHereArgs;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnVirtualHereBrowse;
        private System.Windows.Forms.TextBox tbVirtualHereLocation;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox cbVirtualHereEnabled;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox tbBorderlessGamingArgs;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnBorderlessGamingBrowse;
        private System.Windows.Forms.TextBox tbBorderlessGamingLocation;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox cbBorderlessGamingEnabled;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox tbZwiftLauncherArgs;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnZwiftLauncherBrowse;
        private System.Windows.Forms.TextBox tbZwiftLauncherLocation;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnZwiftAppBrowse;
        private System.Windows.Forms.TextBox tbZwiftAppLocation;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.CheckBox cbAntDebugLogEnabled;
        private System.Windows.Forms.CheckBox cbLedsEnabled;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbPowerMeterDisplay;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnPowerMeterChoose;
        private System.Windows.Forms.CheckBox cbAntDebugWindowEnabled;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown numFtpWatts;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btnLedChoose;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tbLedStripDisplay;
        private System.Windows.Forms.TextBox tbLedBulbDisplay;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.LinkLabel lblUsbIdInfo;
        private System.Windows.Forms.NumericUpDown numUsbId;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.CheckBox cbLedDebugLogEnabled;
        private System.Windows.Forms.CheckBox cbLedDebugWindowEnabled;
    }
}