﻿using System;
using System.Collections.Generic;

namespace ZwiftBabysitter
{
    public class AntPowerMeterDevice
    {
        //From https://www.thisisant.com/developer/resources/tech-bulletin/manufacturer-id -- last updated Oct 14, 2021
        private static readonly Dictionary<int, String> knownManufacturers = new Dictionary<int, String>()
        {
            { 1, "Garmin" },
            { 2, "Garmin Fr405 Antfs" },
            { 3, "Zephyr" },
            { 4, "Dayton" },
            { 5, "Idt" },
            { 6, "Srm" },
            { 7, "Quarq" },
            { 8, "Ibike" },
            { 9, "Saris" },
            { 10, "Spark Hk" },
            { 11, "Tanita" },
            { 12, "Echowell" },
            { 13, "Dynastream Oem" },
            { 14, "Nautilus" },
            { 15, "Dynastream" },
            { 16, "Timex" },
            { 17, "Metrigear" },
            { 18, "Xelic" },
            { 19, "Beurer" },
            { 20, "Cardiosport" },
            { 21, "A And D" },
            { 22, "Hmm" },
            { 23, "Suunto" },
            { 24, "Thita Elektronik" },
            { 25, "Gpulse" },
            { 26, "Clean Mobile" },
            { 27, "Pedal Brain" },
            { 28, "Peaksware" },
            { 29, "Saxonar" },
            { 30, "Lemond Fitness" },
            { 31, "Dexcom" },
            { 32, "Wahoo Fitness" },
            { 33, "Octane Fitness" },
            { 34, "Archinoetics" },
            { 35, "The Hurt Box" },
            { 36, "Citizen Systems" },
            { 37, "Magellan" },
            { 38, "Osynce" },
            { 39, "Holux" },
            { 40, "Concept2" },
            { 41, "Shimano" },
            { 42, "One Giant Leap" },
            { 43, "Ace Sensor" },
            { 44, "Brim Brothers" },
            { 45, "Xplova" },
            { 46, "Perception Digital" },
            { 47, "Bf1Systems" },
            { 48, "Pioneer" },
            { 49, "Spantec" },
            { 50, "Metalogics" },
            { 51, "4Iiiis" },
            { 52, "Seiko Epson" },
            { 53, "Seiko Epson Oem" },
            { 54, "Ifor Powell" },
            { 55, "Maxwell Guider" },
            { 56, "Star Trac" },
            { 57, "Breakaway" },
            { 58, "Alatech Technology Ltd" },
            { 59, "Mio Technology Europe" },
            { 60, "Rotor" },
            { 61, "Geonaute" },
            { 62, "Id Bike" },
            { 63, "Specialized" },
            { 64, "Wtek" },
            { 65, "Physical Enterprises" },
            { 66, "North Pole Engineering" },
            { 67, "Bkool" },
            { 68, "Cateye" },
            { 69, "Stages Cycling" },
            { 70, "Sigmasport" },
            { 71, "Tomtom" },
            { 72, "Peripedal" },
            { 73, "Wattbike" },
            { 76, "Moxy" },
            { 77, "Ciclosport" },
            { 78, "Powerbahn" },
            { 79, "Acorn Projects Aps" },
            { 80, "Lifebeam" },
            { 81, "Bontrager" },
            { 82, "Wellgo" },
            { 83, "Scosche" },
            { 84, "Magura" },
            { 85, "Woodway" },
            { 86, "Elite" },
            { 87, "Nielsen Kellerman" },
            { 88, "Dk City" },
            { 89, "Tacx" },
            { 90, "Direction Technology" },
            { 91, "Magtonic" },
            { 92, "1Partcarbon" },
            { 93, "Inside Ride Technologies" },
            { 94, "Sound Of Motion" },
            { 95, "Stryd" },
            { 96, "Icg" },
            { 97, "Mipulse" },
            { 98, "Bsx Athletics" },
            { 99, "Look" },
            { 100, "Campagnolo Srl" },
            { 101, "Body Bike Smart" },
            { 102, "Praxisworks" },
            { 103, "Limits Technology" },
            { 104, "Topaction Technology" },
            { 105, "Cosinuss" },
            { 106, "Fitcare" },
            { 107, "Magene" },
            { 108, "Giant Manufacturing Co" },
            { 109, "Tigrasport" },
            { 110, "Salutron" },
            { 111, "Technogym" },
            { 112, "Bryton Sensors" },
            { 113, "Latitude Limited" },
            { 114, "Soaring Technology" },
            { 115, "Igpsport" },
            { 116, "Thinkrider" },
            { 117, "Gopher Sport" },
            { 118, "Waterrower" },
            { 119, "Orangetheory" },
            { 120, "Inpeak" },
            { 121, "Kinetic" },
            { 122, "Johnson Health Tech" },
            { 123, "Polar Electro" },
            { 124, "Seesense" },
            { 125, "Nci Technology" },
            { 126, "Iqsquare" },
            { 127, "Leomo" },
            { 128, "Ifit Com" },
            { 129, "Coros Byte" },
            { 130, "Versa Design" },
            { 131, "Chileaf" },
            { 132, "Cycplus" },
            { 133, "Gravaa Byte" },
            { 134, "Sigeyi" },
            { 135, "Coospo" },
            { 136, "Geoid" },
            { 255, "Development" },
            { 257, "Healthandlife" },
            { 258, "Lezyne" },
            { 259, "Scribe Labs" },
            { 260, "Zwift" },
            { 261, "Watteam" },
            { 262, "Recon" },
            { 263, "Favero Electronics" },
            { 264, "Dynovelo" },
            { 265, "Strava" },
            { 266, "Precor" },
            { 267, "Bryton" },
            { 268, "Sram" },
            { 269, "Navman" },
            { 270, "Cobi" },
            { 271, "Spivi" },
            { 272, "Mio Magellan" },
            { 273, "Evesports" },
            { 274, "Sensitivus Gauge" },
            { 275, "Podoon" },
            { 276, "Life Time Fitness" },
            { 277, "Falco E Motors" },
            { 278, "Minoura" },
            { 279, "Cycliq" },
            { 280, "Luxottica" },
            { 281, "Trainer Road" },
            { 282, "The Sufferfest" },
            { 283, "Fullspeedahead" },
            { 284, "Virtualtraining" },
            { 285, "Feedbacksports" },
            { 286, "Omata" },
            { 287, "Vdo" },
            { 288, "Magneticdays" },
            { 289, "Hammerhead" },
            { 290, "Kinetic By Kurt" },
            { 291, "Shapelog" },
            { 292, "Dabuziduo" },
            { 293, "Jetblack" },
            { 294, "Coros" },
            { 295, "Virtugo" },
            { 296, "Velosense" },
            { 297, "Cycligentinc" },
            { 298, "Trailforks" },
            { 299, "Mahle Ebikemotion" },
            { 300, "Nurvv" },
            { 301, "Microprogram" },
            { 302, "Zone5Cloud" },
            { 303, "Greenteg" },
            { 304, "Yamaha Motors" },
            { 305, "Whoop" },
            { 306, "Gravaa" },
            { 307, "Onelap" },
            { 308, "Monark Exercise" },
            { 309, "Form" },
            { 310, "Decathlon" },
            { 311, "Syncros" },
            { 5759, "Actigraphcorp" }
        };

        //Constants for all ANT+ communication
        public static readonly byte[] networkKey = { 0xB9, 0xA5, 0x21, 0xFB, 0xBD, 0x72, 0xC3, 0x45 }; //ANT+ Network Key (as opposed to other ANT networks)
        public static readonly byte networkNum = 0;         //The network key is assigned to this network number

        //Constants for all ANT+ Bike Power Sensors
        public static readonly byte deviceTypeId = 11;      //Device type (per ANT+ spec, 11 is Bike Power Sensor)
        public static readonly byte radioFrequency = 57;    //RF Frequency + 2400 MHz (2457 MHz corresponds to ANT+ sensors)
        public static readonly ushort channelPeriod = 8182; //Channel Period (8182/32768)s period is approximately 4Hz (standard for ANT+ Bike Power Sensor profile)

        //Specific to a particular ANT device
        public int deviceNumber = -1;                       //Can be anything - specific to device.
        public int transmissionTypeId = -1;                 //Can be anything - specific to device. Seems to always be 5 for Bike Power Sensor?

        //Specific to a particular ANT device, but read from ANT page data
        public int manufacturerId = -1;                     //Comes from ANT Common Data Page 80 (0x50)

        public AntPowerMeterDevice() { }
        public AntPowerMeterDevice(int deviceNumber, int transmissionTypeId) : this(deviceNumber, transmissionTypeId, -1) { }
        public AntPowerMeterDevice(int deviceNumber, int transmissionTypeId, int manufacturerId)
        {
            this.deviceNumber = deviceNumber;
            this.transmissionTypeId = transmissionTypeId;
            this.manufacturerId = manufacturerId;
        }

        public String GetPrettyFormat()
        {
            if (deviceNumber < 0 || transmissionTypeId < 0)
                return "(none)";

            if (manufacturerId > -1 && knownManufacturers.TryGetValue(manufacturerId, out String manufacturer))
                return deviceNumber + " (" + manufacturer + ")";
            else
                return Convert.ToString(deviceNumber);
        }

        public static int ParseManufacturerId(byte[] antPageData)
        {
            byte manufacturerIdLsb = antPageData[4];   //Least-significant bit of manufacturer ID
            byte manufacturerIdMsb = antPageData[5];   //Most-significant bit of manufacturer ID
            return manufacturerIdMsb << 8 | manufacturerIdLsb;    //Manufacturer ID as an int
        }
    }
}
