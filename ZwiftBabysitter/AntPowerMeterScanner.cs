﻿using ANT_Managed_Library;
using System;

namespace ZwiftBabysitter
{
    internal class AntPowerMeterScanner : AntBase
    {
        private Action<int, AntPowerMeterDevice> newPowerMeterDiscoveredCallback;
        private Action searchStartCallback;
        private Action antErrorCallback;

        private AntPowerMeterDevice[] availablePowerMeters;
        private int activeChannels = 0;

        public AntPowerMeterScanner(Action<int, AntPowerMeterDevice> newPowerMeterDiscoveredCallback, Action searchStartCallback, Action antErrorCallback)
        {
            this.newPowerMeterDiscoveredCallback = newPowerMeterDiscoveredCallback; //Set the callback to notify the caller that we have a new candidate power meter
            this.searchStartCallback = searchStartCallback;
            this.antErrorCallback = antErrorCallback;
        }

        public void StartScan()
        {
            ZBState.Log("Starting ANT+ Scan...");

            try
            {
                InitAntDevice();

                PerformSearch();
                if (searchStartCallback != null)
                    searchStartCallback();
            }
            catch (Exception ex)
            {
                ZBState.Log("Scan failed with exception: \n" + ex.Message);
                StopScan();
                if(antErrorCallback != null)
                    antErrorCallback();
            }
        }

        public void StopScan()
        {
            if (isRunning)
            {
                ZBState.Log("Stopping ANT+ Scan...");
                CleanUpANT();
            }
        }

        private void PerformSearch()
        {
            //Initialize our array of discovered power meters
            availablePowerMeters = new AntPowerMeterDevice[ZBConfig.maxAntSearchChannels];

            //Open first channel for searching
            channels = new ANT_Channel[ZBConfig.maxAntSearchChannels];
            activeChannels = 0;
            OpenAntChannel(activeChannels++, 0, 0);
        }

        protected override void ReceiveAntPageData(byte channelNum, ANT_ChannelID channelId, byte[] pageData)
        {
            //This is a new power meter we're receiving data for. Store it and notify the caller
            if(availablePowerMeters[channelNum] == null)
            {
                ZBState.Log("-=-=- Discovered " + channelId.deviceNumber + "!");
                
                AntPowerMeterDevice newPowerMeter = new AntPowerMeterDevice(channelId.deviceNumber, channelId.transmissionTypeID);
                
                availablePowerMeters[channelNum] = newPowerMeter;
                if (newPowerMeterDiscoveredCallback != null)
                    newPowerMeterDiscoveredCallback(channelNum, newPowerMeter);

                //Open the next search channel
                if (activeChannels < ZBConfig.maxAntSearchChannels)
                    OpenAntChannel(activeChannels++, 0, 0);
            }

            //Page 0x50 contains the manufacturer ID, which we want for display purposes
            if(pageData[0] == 0x50)
            {
                ZBState.Log("-=-=- Got Manufacturer Info for " + channelId.deviceNumber +"!");
                availablePowerMeters[channelNum].manufacturerId = AntPowerMeterDevice.ParseManufacturerId(pageData);
            }
        }

        protected override void ChannelClosed(byte channelNum)
        {
            //Do nothing
        }
    }
}
