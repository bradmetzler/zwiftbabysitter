﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace ZwiftBabysitter
{
    public class DebugWindowBase : Form
    {
        protected static readonly Color defaultColor = Color.FromArgb(255, 200, 200, 200);
        protected static readonly Color zone1Color = Color.FromArgb(255, 127, 127, 127);
        protected static readonly Color zone2Color = Color.FromArgb(255, 55, 138, 245);
        protected static readonly Color zone3Color = Color.FromArgb(255, 90, 190, 90);
        protected static readonly Color zone4Color = Color.FromArgb(255, 248, 204, 68);
        protected static readonly Color zone5Color = Color.FromArgb(255, 237, 99, 52);
        protected static readonly Color zone6Color = Color.FromArgb(255, 236, 49, 35);

        protected Point clickPoint;

        //These mouse event handlers allow the entire window to be click-and-draggable, since we don't have a title bar
        protected virtual void DragWindowMouseDownHandler(object sender, MouseEventArgs e)
        {
            int x = e.X;
            int y = e.Y;

            if (sender.GetType() == typeof(Label))
            {
                x += ((Label)sender).Location.X;
                y += ((Label)sender).Location.Y;
            }

            clickPoint = new Point(x, y);
        }

        //These mouse event handlers allow the entire window to be click-and-draggable, since we don't have a title bar
        protected virtual void DragWindowMouseMoveHandler(object sender, MouseEventArgs e)
        {
            int x = e.X;
            int y = e.Y;

            if (sender.GetType() == typeof(Label))
            {
                x += ((Label)sender).Location.X;
                y += ((Label)sender).Location.Y;
            }

            if (e.Button == MouseButtons.Left)
            {
                this.Left += x - clickPoint.X;
                this.Top += y - clickPoint.Y;
            }
        }

        protected void TransparetBackground(Control C)
        {
            C.Visible = false;

            C.Refresh();
            Application.DoEvents();

            Rectangle screenRectangle = RectangleToScreen(this.ClientRectangle);
            int titleHeight = screenRectangle.Top - this.Top;
            int Right = screenRectangle.Left - this.Left;

            Bitmap bmp = new Bitmap(this.Width, this.Height);
            this.DrawToBitmap(bmp, new Rectangle(0, 0, this.Width, this.Height));
            Bitmap bmpImage = new Bitmap(bmp);
            bmp = bmpImage.Clone(new Rectangle(C.Location.X + Right, C.Location.Y + titleHeight, C.Width, C.Height), bmpImage.PixelFormat);
            C.BackgroundImage = bmp;

            C.Visible = true;
        }

        //These debug windows are sometimes closed from a different thread. This allows that to happen safely.
        public void CloseSafely()
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new Action(CloseSafely));
                return;
            }

            Close();
        }
    }
}
