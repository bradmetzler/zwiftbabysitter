﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;
using static System.Windows.Forms.CheckedListBox;

namespace ZwiftBabysitter
{
    public partial class ChooseLightsWindow : Form
    {
        private List<LedLight> selectedBulbs;
        private List<LedLight> selectedStrips;

        private List<LedLight> availableBulbs;
        private List<LedLight> availableStrips;
        private Thread scannerThread;
        private LedScanner scanner;
        private readonly object ledLightLock = new object();

        private static readonly int scanTimerInterval = 500;    //Milliseconds
        private static readonly int scanTimerLength = 10;       //Seconds
        private long scanStart = 0;
        private System.Windows.Forms.Timer scanTimer;

        public ChooseLightsWindow(List<LedLight> selectedBulbs, List<LedLight> selectedStrips)
        {
            InitializeComponent();

            pbScanProgress.Maximum = scanTimerLength;
            
            this.selectedBulbs = selectedBulbs;
            this.selectedStrips = selectedStrips;

            availableBulbs = new List<LedLight>();
            availableStrips = new List<LedLight>();

            scanTimer = new System.Windows.Forms.Timer();
            scanTimer.Interval = scanTimerInterval;
            scanTimer.Tick += new EventHandler(ScanTimerTick);
            scanTimer.Start();
            scanStart = ZBState.CurrentTimeMillis();

            //LED Scanning blocks waiting for UDP packets. Start the scanner in its own thread so that the UI doesn't hang
            Log("Starting the LedScanner!");
            lblScanProgress.Text = "Scanning for Yeelight devices...";
            scanner = new LedScanner(NewBulbDiscovered, NewStripDiscovered);
            scannerThread = new Thread(new ThreadStart(scanner.Scan));
            scannerThread.Start();
        }

        private void ScanTimerTick(Object myObject, EventArgs myEventArgs)
        {
            long currentTime = ZBState.CurrentTimeMillis();
            short elapsedSeconds = (short)((currentTime - scanStart) / 1000);

            if(elapsedSeconds >= scanTimerLength)
            {
                lblScanProgress.Text = "Done Scanning";
                pbScanProgress.Value = scanTimerLength;
                StopScanning();
            }
            else
            {
                lblScanProgress.Text = "Scanning for " + (scanTimerLength - elapsedSeconds) + " more seconds...";
                pbScanProgress.Value = elapsedSeconds;
            }

            //Refresh the on-screen lists of available LED bulbs & strips
            lock (ledLightLock)
            {
                //Update the list of available bulbs
                if(availableBulbs.Count > listAvailableBulbs.Items.Count)
                {
                    for(int i = listAvailableBulbs.Items.Count; i < availableBulbs.Count; i++)
                        listAvailableBulbs.Items.Add(availableBulbs[i].GetDetailFormat());
                }

                //Update the list of available strips
                if (availableStrips.Count > listAvailableStrips.Items.Count)
                {
                    for (int i = listAvailableStrips.Items.Count; i < availableStrips.Count; i++)
                        listAvailableStrips.Items.Add(availableStrips[i].GetDetailFormat());
                }
            }
        }

        private void StopScanning()
        {
            if(scanTimer != null)
                scanTimer.Stop();

            //Signal the scanner thread to stop scanning
            if (scannerThread != null && scannerThread.IsAlive)
                scanner.AbortScan();
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            StopScanning();

            selectedBulbs.Clear();
            selectedStrips.Clear();

            CheckedIndexCollection checkedBulbIndices = listAvailableBulbs.CheckedIndices;
            if(checkedBulbIndices.Count > 0)
            {
                for(int i = 0; i < checkedBulbIndices.Count; i++)
                    selectedBulbs.Add(availableBulbs[checkedBulbIndices[i]]);
            }

            CheckedIndexCollection checkedStripIndices = listAvailableStrips.CheckedIndices;
            if (checkedStripIndices.Count > 0)
            {
                for (int i = 0; i < checkedStripIndices.Count; i++)
                    selectedStrips.Add(availableStrips[checkedStripIndices[i]]);
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            StopScanning();

            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        public void NewBulbDiscovered(LedLight bulb)
        {
            lock(ledLightLock)
            {
                availableBulbs.Add(bulb);
            }
        }

        public void NewStripDiscovered(LedLight strip)
        {
            lock (ledLightLock)
            {
                availableStrips.Add(strip);
            }
        }

        private void ChooseLightsWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            StopScanning();
        }

        private void LightList_Click(object sender, EventArgs e)
        {
            MouseEventArgs m = (MouseEventArgs)e;
            CheckedListBox l = (CheckedListBox)sender;

            int clickedIndex = l.IndexFromPoint(m.X, m.Y);
            
            if(clickedIndex >= 0 && clickedIndex < l.Items.Count)
                l.SetItemChecked(clickedIndex, !l.GetItemChecked(clickedIndex));

            btnOk.Enabled = listAvailableBulbs.CheckedIndices.Count > 0 || listAvailableStrips.CheckedIndices.Count > 0;
        }

        private void Log(String message)
        {
            //Most logging from this class is considered debug logging, so only log it if the user wants to see it
            if (ZBConfig.ledDebugLogEnabled)
                ZBState.Log(message);
        }
    }
}
