Zwift Babysitter Notes
======================

Possible features to add in the future:
- Gear Position and Battery ANT+ profile monitoring

For lighting based on power zones, the following lighting options were considered:
1. Arduino with WS2812B LED strips
	Pros: More learning opportunity, exposure to Arduino.
	Cons: Requires some tools I don't currently have. Powering the WS2812B strips requires DC power supplies that are apparently semi-questionable and could cause a fire? Don't want to burn the house down.
2. Blinkstick Flex
	Pros: Simple, capable API. No custom wiring required.
	Cons: Quite a bit of space between individual LEDs. Likely requires a powered USB hub, and even then is limited to 500mA per strip.
3. Blinkstick Flex Board
	Pros: Same API as Flex, but with whatever WS2812B strip(s) I prefer.
	Cons: Requires a bit of soldering (not a big deal). Same power concerns as the Flex.
4. Philips Hue
	Pros: Can also get a controllable light bulb. No power issues.
	Cons: Expensive, and only truly controllable if you also buy a Hue Hub. ~$200 for hub, 1 bulb, and 1 strip
5. LIFX
	Pros: Can get a bulb. Cheaper than Hue. No power issues.
	Cons: API seems powerful but may have been disabled or neutered in a recent firmware
6. Nonoleaf
	Pros: Customizable patterns out of various shaped LED tiles. API seems powerful.
	Cons: Expensive (possibly even more than Hue). Nanoleaf "Essentials" bulbs and strips that I want may require "Thread" which requires also running a border router.
7. Yeelight
	Pros: No hub required. No power issues. Can get a bulb and a strip. One of the cheapest options. API looks pretty good. API is totally on the LAN and doesn't depend on a cloud service at all.
	Cons: Cannot individually address each LED on the strip (though the API does allow for pretty customizable patterns/transitions/flows).
Ultimately, I chose Yeelight. I'm leaving this list here in case Yeelight doesn't work as expected or other options are considered in the future.