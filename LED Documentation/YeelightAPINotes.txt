telnet 192.168.4.202 55443
--------------------------
{"id": 489, "method": "set_name", "params": ["PainCave LightBulb"]}

telnet 192.168.4.203 55443
--------------------------
{"id": 489, "method": "set_name", "params": ["PainCave LightStrip"]}		Set the name
{"id": 489, "method": "set_power", "params": ["on", "smooth", 250]}			Turn on (gradually over 250ms)
{"id": 489, "method": "set_power", "params": ["off", "smooth", 1000]}		Turn off (gradually over 1s)
{"id": 489, "method": "toggle", "params": []}								Toggle on/off
{"id": 489, "method": "set_bright", "params": [100, "sudden", 0]}			Change brightness to 100%
{"id": 489, "method": "set_bright", "params": [50, "smooth", 500]}			Change brightness to 50%
{"id": 489, "method": "set_rgb", "params": [16711680, "sudden", 0]}			Immediately go to red
{"id": 489, "method": "set_rgb", "params": [16753920, "sudden", 0]}			Immediately go to orange
{"id": 489, "method": "set_rgb", "params": [16776960, "sudden", 0]}			Immediately go to yellow
{"id": 489, "method": "set_rgb", "params": [65280, "sudden", 0]}			Immediately go to green
{"id": 489, "method": "set_rgb", "params": [255, "sudden", 0]}				Immediately go to blue
{"id": 489, "method": "set_rgb", "params": [10494192, "sudden", 0]}			Immediately go to purple
{"id": 489, "method": "set_rgb", "params": [16716011, "sudden", 0]}			Immediately go to pink
{"id": 489, "method": "set_rgb", "params": [16777215, "sudden", 0]}			Immediately go to white

Get iniital state
{"id": 489, "method": "get_prop", "params": ["power","bright","ct","rgb","hue","sat","color_mode","flowing","flow_params"]}

Rainbow at 100% brightness and stop on white at 50% brightness
{"id": 489, "method": "start_cf", "params": [9, 1, "200,1,16711680,100,200,1,16753920,100,200,1,16776960,100,200,1,65280,100,200,1,255,100,200,1,10494192,100,200,1,16716011,100,200,1,16777215,100,200,1,16777215,50"]}

Slow red blink (connection error)
{"id": 489, "method": "start_cf", "params": [0, 1, "1000,1,0,1,1500,7,0,0,50,1,16711680,50"]}

Slow pink heartbeat (config error)
{"id": 489, "method": "start_cf", "params": [0, 1, "50,1,16716011,25,3500,7,0,0,50,1,16716011,50,50,7,0,0,50,1,16716011,25,50,7,0,0,50,1,16716011,50,50,7,0,0"]}

Zone 0 (nothing)
----------------
Strip
Solid white 50% brightness

Bulb
100% brightness at 5000K

Zone 1 (gray)
-------------
Strip
Solid white 50% brightness

Bulb - 50% brightness 5000K
{"id": 489, "method": "start_cf", "params": [1, 1, "500,2,5000,50"]}

Zone 2 (blue)
-------------
Strip - solid blue 50% brightness
{"id": 489, "method": "start_cf", "params": [1, 1, "500,1,3217663,50"]}

Bulb - 50% brightness 5000K
{"id": 489, "method": "start_cf", "params": [1, 1, "500,2,5000,50"]}

Zone 3 (green)
--------------
Strip - solid green 50% brightness
{"id": 489, "method": "start_cf", "params": [1, 1, "500,1,65280,50"]}

Bulb - 50% brightness 5000K
{"id": 489, "method": "start_cf", "params": [1, 1, "500,2,5000,50"]}

Zone 4 (yellow)
---------------
Strip - solid yellow 75% brightness
{"id": 489, "method": "start_cf", "params": [1, 1, "500,1,16776960,75"]}

Bulb - 100% brightness 2700K
{"id": 489, "method": "start_cf", "params": [1, 1, "500,2,2700,100"]}

Zone 5 (orange)
---------------
Strip - 2-second orange heartbeat 50-80% brightness
{"id": 489, "method": "start_cf", "params": [0, 1, "50,1,16753920,50,1700,7,0,0,50,1,16753920,80,50,7,0,0,50,1,16753920,50,50,7,0,0,50,1,16753920,80,50,7,0,0"]}

Bulb - 100% brightness 2700K
{"id": 489, "method": "start_cf", "params": [1, 1, "500,2,2700,100"]}

Zone 6 (red)
------------
Strip - half-second red breathe 50-80% brightness
{"id": 489, "method": "start_cf", "params": [0, 1, "50,1,16711680,50,450,7,0,0,250,1,16711680,80,100,7,0,0"]}

Bulb - 100% brightness red
{"id": 489, "method": "start_cf", "params": [1, 1, "500,1,16711680,100"]}